package com.nkunku.javaexercises.log4j.mdc;

import org.apache.logging.log4j.*;

import com.nkunku.javaexercises.MDC;

/**
 * Unique package class.
 */
public final class Main {

	/**
	 * Cannot be instantiated.
	 */
	private Main() {}

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(Main.class);

	public static void main(final String[] args) {
		ThreadContext.put(MDC.USER_NAME.id(), "Mike Nkunku");
		ThreadContext.put(MDC.USER_EMAIL.id(), "mikenkunku@gmail.com");
		if (logger.isInfoEnabled()) {
			logger.info("Logging some info...");
		}

		ThreadContext.put(MDC.USER_NAME.id(), "mikemmg1");
		if (logger.isInfoEnabled()) {
			logger.info("Logging some info without cleaning context...");
		}

		ThreadContext.clearMap();
		ThreadContext.put(MDC.USER_NAME.id(), "Mike Nkunku");
		ThreadContext.put(MDC.DATE.id(), "2022-02-15");
		ThreadContext.put(MDC.MISCELLANEOUS.id(), "Hello, shonen!");
		if (logger.isInfoEnabled()) {
			logger.info("Logging some info after cleaning context...");
		}
	}
}
