package com.nkunku.javaexercises.codeexercises;

import java.time.Duration;
import java.time.Instant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nkunku.javaexercises.MDC;

public final class LargestPrimeFactor {

	private static final Logger logger = LogManager.getLogger(LargestPrimeFactor.class);

	private LargestPrimeFactor() {}

	public static void main(final String[] args) {
		final int number = 13_195;

		final Instant start = Instant.now();
		final Integer largestPrimeFactor = getLargestPrimeFactor(number);
		MDC.logExecutionTime(Duration.between(start, Instant.now()));

		if (logger.isInfoEnabled()) {
			logger.info("Largest prime factor of {}: {}", Integer.valueOf(number), largestPrimeFactor);
		}
	}

	private static Integer getLargestPrimeFactor(final int number) {
		for (int i = number - 1; i > 1; i--) {
			if (number % i == 0) {
				return getLargestPrimeFactor(i);
			}
		}
		return Integer.valueOf(number);
	}
}
