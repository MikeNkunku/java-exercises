package com.nkunku.javaexercises.codeexercises;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

final class LongestPalindromeFinder {

	private LongestPalindromeFinder() {}

	static String find(final String word) {
		if (new StringBuilder(word).reverse().toString().equals(word)) {
			return word;
		}

		final Map<String, List<Integer>> map = new HashMap<>(word.length());
		for (int i = 0; i < word.length(); i++) {
			final String key = String.valueOf(word.charAt(i));
			if (map.containsKey(key)) {
				map.get(key).add(Integer.valueOf(i));
			} else {
				final List<Integer> newList = new ArrayList<>(2);
				newList.add(Integer.valueOf(i));
				map.put(key, newList);
			}
		}
		final List<Entry<String, List<Integer>>> duplicatedLetters = map.entrySet().stream()
				.filter(entry -> entry.getValue().size() > 1).collect(Collectors.toList());
		if (duplicatedLetters.isEmpty()) {
			return String.valueOf(word.charAt(0));
		}

		String longestPalindrome = "";
		for (Entry<String, List<Integer>> entry : duplicatedLetters) {
			List<Integer> indexes = entry.getValue();
			String subword = word.substring(indexes.get(0).intValue(), indexes.get(indexes.size() - 1).intValue() + 1);
			if (new StringBuilder(subword).reverse().toString().equals(subword)
					&& longestPalindrome.length() < subword.length()) {
				longestPalindrome = subword;
			}
		}

		return longestPalindrome;
	}
}
