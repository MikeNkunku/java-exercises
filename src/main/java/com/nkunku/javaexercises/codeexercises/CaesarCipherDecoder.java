package com.nkunku.javaexercises.codeexercises;

final class CaesarCipherDecoder {

	private static final int ROTATION = 5;
	private static final char ENDING_LETTER = 'z';
	private static final int ALPHABET_LENGTH = 26;

	private CaesarCipherDecoder() {}

	public static String decode(final String code) {
		final StringBuilder builder = new StringBuilder(code.length());
		for (char c : code.toCharArray()) {
			int charIdx = c + ROTATION;
			if (charIdx > ENDING_LETTER) {
				charIdx -= ALPHABET_LENGTH;
			}
			builder.append((char) charIdx);
		}
		return builder.toString();
	}
}
