package com.nkunku.javaexercises.codeexercises;

import java.time.Duration;
import java.time.Instant;

import org.apache.logging.log4j.*;

import com.nkunku.javaexercises.MDC;

/**
 * Class finding the associated Fibonacci number based on the provided 0-starting index.
 */
public final class FibonacciNumber {

	/** Cannot be instantiated. */
	private FibonacciNumber() {}

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(FibonacciNumber.class);

	public static void main(final String[] args) {
		final int index = 28;
		final Instant start = Instant.now();
		final Integer fibonacciResult = Integer.valueOf(fibonacci(index));
		MDC.logExecutionTime(Duration.between(start, Instant.now()));
		if (logger.isInfoEnabled()) {
			logger.info("Fibonacci for {}: {}", Integer.valueOf(index), fibonacciResult);
		}
	}

	private static int fibonacci(final int idx) {
		if (idx == 0 || idx == 1) {
			return idx;
		}
		return fibonacci(idx - 1) + fibonacci(idx - 2);
	}
}
