package com.nkunku.javaexercises.codeexercises;

import java.time.Duration;
import java.time.Instant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nkunku.javaexercises.MDC;

public final class RiceBagPackageChecker2 {

	private static final Logger logger = LogManager.getLogger(RiceBagPackageChecker2.class);

	private RiceBagPackageChecker2() {}

	public static void main(final String[] args) {
		final int nbSmall = 5;
		final int nbBig = 2;
		final int goal = 6;

		final Instant start = Instant.now();
		final int minimumNumberOfSmallBags = getMinimumNumberOfSmallBags(nbSmall, nbBig, goal);
		MDC.logExecutionTime(Duration.between(start, Instant.now()));

		if (logger.isInfoEnabled()) {
			if (minimumNumberOfSmallBags == -1) {
				logger.info("Not possible to package {} kilos of rice with {} small bags & {} big ones",
						Integer.valueOf(goal), Integer.valueOf(nbSmall), Integer.valueOf(nbBig));
			} else {
				logger.info("Minimum number of small bags for {} kilos with {} small bags & " +
						"{} big ones: {}", Integer.valueOf(goal), Integer.valueOf(nbSmall), Integer.valueOf(nbBig),
						Integer.valueOf(minimumNumberOfSmallBags));
			}
		}
	}

	private static int getMinimumNumberOfSmallBags(final int nbSmall, final int nbBig, final int goal) {
		if (goal == 0) {
			return 0;
		}

		if (goal < 5 && nbSmall >= goal) {
			return goal;
		}

		if (goal % 5 == 0 && nbBig >= (goal / 5)) {
			return 0;
		}

		int g = goal;
		g -= Math.min(nbBig * 5, (goal / 5) * 5);
		final int minSmallBags = Math.min(nbSmall, g);
		g -= minSmallBags;

		return g == 0 ? minSmallBags : -1;
	}
}
