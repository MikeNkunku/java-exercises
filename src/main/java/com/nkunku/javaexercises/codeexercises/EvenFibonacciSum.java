package com.nkunku.javaexercises.codeexercises;

import java.time.Duration;
import java.time.Instant;

import org.apache.logging.log4j.*;

import com.nkunku.javaexercises.MDC;

/**
 * Class finding the associated Fibonacci number based on the provided 0-starting index.
 */
public final class EvenFibonacciSum {

	/** Cannot be instantiated. */
	private EvenFibonacciSum() {}

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(EvenFibonacciSum.class);

	public static void main(final String[] args) {
		final int number = 8;
		final Instant start = Instant.now();
		final Integer fibonacciSumResult = Integer.valueOf(evenFibonacciSum(number));
		MDC.logExecutionTime(Duration.between(start, Instant.now()));
		if (logger.isInfoEnabled()) {
			logger.info("Fibonacci sum for N equal or lower than {}: {}", Integer.valueOf(number), fibonacciSumResult);
		}
	}

	private static int evenFibonacciSum(final int number) {
		int evenSum = 0;
		int previous = 0;
		int current = 1;

		do {
			if (current % 2 == 0) {
				evenSum += current;
			}
			int newFibonacciNumber = current + previous;
			previous = current;
			current = newFibonacciNumber;
		} while (current < number);

		return evenSum;
	}
}
