package com.nkunku.javaexercises.codeexercises;

import java.time.Duration;
import java.time.Instant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nkunku.javaexercises.MDC;

public final class RiceBagPackageChecker {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(RiceBagPackageChecker.class);

	/** Cannot be instantiated. */
	private RiceBagPackageChecker() {}

	public static void main(final String[] args) {
		final int nbSmall = 5;
		final int nbBig = 2;
		final int goal = 16;

		final Instant start = Instant.now();
		final Boolean canRiceBePackaged = canRiceBePackaged(nbSmall, nbBig, goal);
		MDC.logExecutionTime(Duration.between(start, Instant.now()));

		if (logger.isInfoEnabled()) {
			logger.info("Is it possible to have a rice package of {} kilos with {} small bags & " +
					"{} big ones? {}", Integer.valueOf(goal), Integer.valueOf(nbSmall), Integer.valueOf(nbBig),
					canRiceBePackaged);
		}
	}

	private static Boolean canRiceBePackaged(final int nbSmall, final int nbBig, final int goal) {
		if (goal == 0) {
			return Boolean.TRUE;
		}

		if (goal < 5 && nbSmall >= goal) {
			return Boolean.TRUE;
		}

		if (goal % 5 == 0 && nbBig >= (goal / 5)) {
			return Boolean.TRUE;
		}

		int g = goal;
		g -= Math.min(nbBig * 5, (goal / 5) * 5);
		g -= Math.min(nbSmall, g);

		return Boolean.valueOf(g == 0);
	}
}
