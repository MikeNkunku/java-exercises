package com.nkunku.javaexercises.codeexercises;

import java.time.Duration;
import java.time.Instant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nkunku.javaexercises.MDC;

public final class GreatestCommonDivisor {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(GreatestCommonDivisor.class);

	/** Cannot be instantiated. */
	private GreatestCommonDivisor() {}

	public static void main(final String[] args) {
		int p = 26;
		int q = 12;
		final Instant start = Instant.now();
		MDC.logExecutionTime(Duration.between(start, Instant.now()));
		if (logger.isInfoEnabled()) {
			logger.info("The GCD for {} & {} is: {}", Integer.valueOf(p), Integer.valueOf((q)), getGCD(p, q));
		}
	}

	private static Integer getGCD(final int p, final int q) {
		int divider = Math.min(p, q);
		if (divider == 0) {
			return Integer.valueOf(Math.max(p, q));
		}

		while (p % divider != 0 || q % divider != 0) {
			--divider;
		}
		return Integer.valueOf(divider);
	}
}
