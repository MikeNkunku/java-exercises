package com.nkunku.javaexercises.codeexercises;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nkunku.javaexercises.MDC;

public final class StringFilter {

	private static final Logger logger = LogManager.getLogger(StringFilter.class);

	private static final char STARTING_LETTER = 'a';
	private static final int TARGETED_LENGTH = 3;

	private StringFilter() {}

	public static void main(final String[] args) {
		final List<String> initialList = Arrays.asList("abc", "arbitrary", "decade", "long", "yellow", "add", "ape",
				"awesome");
		final Instant start = Instant.now();
		final List<String> filteredList = filterList(initialList);
		MDC.logExecutionTime(Duration.between(start, Instant.now()));

		if (logger.isInfoEnabled()) {
			logger.info("Initial list: {};Filtered list: {}", initialList, filteredList);
		}
	}

	private static List<String> filterList(final List<String> initialList) {
		return initialList.stream().filter(word -> !word.isEmpty() && word.charAt(0) == STARTING_LETTER
				&& word.length() == TARGETED_LENGTH).collect(Collectors.toList());
	}
}
