package com.nkunku.javaexercises.codeexercises;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nkunku.javaexercises.MDC;

public final class CommaSeparatedIntegerFormatter {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(CommaSeparatedIntegerFormatter.class);

	/** Cannot be instantiated. */
	private CommaSeparatedIntegerFormatter() {}

	public static void main(final String[] args) {
		final List<Integer> list = Arrays.asList(Integer.valueOf(3), Integer.valueOf(44), Integer.valueOf(125),
				Integer.valueOf(20), Integer.valueOf(45));

		final Instant start = Instant.now();
		final String formattedString = getFormattedString(list);
		MDC.logExecutionTime(Duration.between(start, Instant.now()));

		if (logger.isInfoEnabled()) {
			logger.info("Initial list: {};Formatted String: {}", list, formattedString);
		}
	}

	private static String getFormattedString(final List<Integer> list) {
		return list.stream().mapToInt(Integer::intValue).mapToObj(i -> (i % 2 == 0 ? "e" : "o") + i)
				.collect(Collectors.joining(","));
	}


}
