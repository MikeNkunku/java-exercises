package com.nkunku.javaexercises.codeexercises;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nkunku.javaexercises.MDC;

public final class TwoSumTargetChecker {

	private static final Logger logger = LogManager.getLogger(TwoSumTargetChecker.class);

	private TwoSumTargetChecker() {}

	public static void main(final String[] args) {
		final int[] arr = new int[] {1, 3, 4};
		final int target = 5;

		final Instant start = Instant.now();
		final Boolean canSumToTarget = canSumToTarget(arr, target);
		MDC.logExecutionTime(Duration.between(start, Instant.now()));

		if (logger.isInfoEnabled()) {
			logger.info("Can two elements be summed in {} to reach {}? {}", Arrays.toString(arr), Integer.valueOf(target),
					canSumToTarget);
		}
	}

	private static Boolean canSumToTarget(final int[] arr, final int target) {
		final HashMap<Integer, Integer> complementMap = new HashMap<>(arr.length);
		for (int i = 0; i < arr.length; i++) {
			int complement = target - arr[i];
			if (complementMap.containsValue(Integer.valueOf(complement))) {
				return Boolean.TRUE;
			}
			complementMap.put(Integer.valueOf(i), Integer.valueOf(arr[i]));
		}
		return Boolean.FALSE;
	}
}
