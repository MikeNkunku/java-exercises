package com.nkunku.javaexercises.source00.dateandtime;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.Temporal;
import java.util.EnumMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which takes as unique parameter a duration which is to be between 10 seconds and {@link java.lang.Long#MAX_VALUE Long#MAX_VALUE}
 * and simulates a chess game.
 */
public final class ClockBasedChessGameSimulator {

	/** Minimum duration allowed for a chess game. */
	private static final long MIN_DURATION = 10L;
	private static final long NUMBER_OF_MILLISECONDS_IN_A_SECOND = 1000L;

	private static final Logger logger = LogManager.getLogger(ClockBasedChessGameSimulator.class);

	public static void main(final String[] args) throws InterruptedException {
		if (ArrayUtils.getLength(args) != 1) {
			logger.warn("Invalid input: only one parameter must be provided.");
			return;
		}

		final long gameDuration;
		try {
			gameDuration = Long.parseLong(args[0]);
		} catch (final NumberFormatException e) {
			logger.warn("The provided duration could not be parsed.");
			return;
		}

		if (gameDuration < MIN_DURATION) {
			logger.warn("Invalid duration: it must be at least 10 (seconds).");
			return;
		}

		simulateChessGame(gameDuration);
	}

	private static void simulateChessGame(long gameDuration) throws InterruptedException {
		final ChessClock chessClock = new ChessClock(Players.LEFT);
		long duration;
		do {
			duration = (long) (Math.random() * gameDuration + 1.0);
			gameDuration -= duration;
			Thread.sleep(duration * NUMBER_OF_MILLISECONDS_IN_A_SECOND);
			chessClock.instant();
			logPlayerTurn(chessClock, gameDuration);
		} while (gameDuration > 0L);
	}

	private static void logPlayerTurn(final ChessClock chessClock, final long gameDuration) {
		final Players playerToLog = Players.LEFT.equals(chessClock.currentPlayer) ? Players.RIGHT : Players.LEFT;
		final StringBuilder stringBuilder = new StringBuilder(playerToLog.name()).append(" took {} seconds to play");
		if (gameDuration == 0L) {
			stringBuilder.append(" and won");
		}
		logger.info(stringBuilder.append('.').toString(), Long.valueOf(chessClock.lastTurnDurations.get(playerToLog).getSeconds()));
	}

	private enum Players {
		LEFT,
		RIGHT
	}

	static class ChessClock extends Clock {

		/** The player who is currently thinking. */
		private Players currentPlayer;
		/** Map holding the last time each of the players played. */
		private final Map<Players, Instant> lastInstants = new EnumMap<>(Players.class);
		/** Map holding for each player the duration of her/his last turn.<br>
		 * A turn duration for a player is here considered to be the thinking time during the other player's turn + the time taken for the player to
		 * make her/his move. */
		private final Map<Players, Duration> lastTurnDurations = new EnumMap<>(Players.class);

		/**
		 * @param player The player who will start the chess game.
		 */
		ChessClock(final Players player) {
			super();
			currentPlayer = player;
			final Instant now = Instant.now();
			for (final Players playerValue : Players.values()) {
				lastInstants.put(playerValue, now);
			}
		}

		@Override
		public ZoneId getZone() {
			return ZoneId.of("UTC+01:00");
		}

		@Override
		public Clock withZone(final ZoneId zoneId) {
			return this;
		}

		@Override
		public Instant instant() {
			final Instant now = Instant.now();
			final Players nextPlayer = getNextPlayer(currentPlayer);
			updatePlayersData(now, nextPlayer);
			currentPlayer = nextPlayer;
			return now;
		}

		private static Players getNextPlayer(final Players currentPlayerValue) {
			return Players.LEFT.equals(currentPlayerValue) ? Players.RIGHT : Players.LEFT;
		}

		private void updatePlayersData(final Instant now, final Players nextPlayer) {
			lastTurnDurations.put(currentPlayer, getTurnDuration(lastInstants.get(currentPlayer), now));
			lastInstants.put(nextPlayer, now);
		}

		private static Duration getTurnDuration(final Temporal turnStartInstant, final Temporal now) {
			return Duration.between(turnStartInstant, now);
		}
	}
}
