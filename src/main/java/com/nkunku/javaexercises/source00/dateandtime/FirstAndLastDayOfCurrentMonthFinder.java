package com.nkunku.javaexercises.source00.dateandtime;

import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which finds the first and last day of the current month.
 */
public final class FirstAndLastDayOfCurrentMonthFinder {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(FirstAndLastDayOfCurrentMonthFinder.class);

	/**
	 * @param arguments The arguments received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isValidInput(arguments)) {
			logger.warn("Invalid input: no argument must be provided.");
			return;
		}

		final ZonedDateTime nowZDT = ZonedDateTime.now();
		final ZonedDateTime firstDayOfMonth = nowZDT.with(TemporalAdjusters.firstDayOfMonth());
		final ZonedDateTime lastDayOfMonth =  nowZDT.with(TemporalAdjusters.lastDayOfMonth());
		logger.info("Current month: {}.\nFirst day of the month: {} {}.\nLast day of the month: {} {}.",
				nowZDT.getMonth(), firstDayOfMonth.getDayOfWeek(), Integer.valueOf(firstDayOfMonth.getDayOfMonth()),
				lastDayOfMonth.getDayOfWeek(), Integer.valueOf(lastDayOfMonth.getDayOfMonth()));
	}

	/**
	 * @param arguments The arguments received from the command line.
	 */
	private static boolean isValidInput(final String[] arguments) {
		return ArrayUtils.getLength(arguments) == 0;
	}
}
