package com.nkunku.javaexercises.source00.dateandtime;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalField;
import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which displays all the time units for the current time.
 */
public final class CurrentTimeUnitsDisplayer {
	private static final Collection<TemporalField> TIME_FIELDS = Arrays.asList(ChronoField.YEAR, ChronoField.MONTH_OF_YEAR, ChronoField.DAY_OF_MONTH,
			ChronoField.HOUR_OF_DAY, ChronoField.MINUTE_OF_HOUR, ChronoField.SECOND_OF_MINUTE, ChronoField.NANO_OF_SECOND);

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(CurrentTimeUnitsDisplayer.class);

	/**
	 * @param arguments Arguments received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isValidInput(arguments)) {
			logger.warn("Invalid input: no argument must be provided.");
			return;
		}

		displayTimeUnits(ZonedDateTime.now());
	}

	/**
	 * @param arguments Arguments received from the command line.
	 */
	private static boolean isValidInput(final String[] arguments) {
		return ArrayUtils.getLength(arguments) == 0;
	}

	/**
	 * @param temporal The temporal instance whose time units must be displayed.
	 */
	private static void displayTimeUnits(final Temporal temporal) {
		final StringBuilder stringBuilder = new StringBuilder("Temporal value: ").append(temporal.toString());
		TIME_FIELDS.stream().filter(temporal::isSupported)
				.forEachOrdered(timeField -> stringBuilder.append('\n').append(timeField.getDisplayName(Locale.ENGLISH)).append(": ")
						.append(temporal.getLong(timeField)));
		logger.info(stringBuilder.toString());
	}
}
