package com.nkunku.javaexercises.source00.dateandtime;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which computes the age of a person by using one's birth in the {@code yyyy-MM-dd} or {@code yyyy-MM-ddZ} format.
 */
public final class AgeCalculator {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(AgeCalculator.class);

	/**
	 * @param arguments Parameters received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isInputValid(arguments)) {
			logger.warn("Invalid argument: only one string must be provided.");
			return;
		}

		final Temporal birthday;
		try {
			birthday = DateTimeFormatter.ISO_DATE.parse(arguments[0], LocalDate::from);
		} catch (final DateTimeParseException e) {
			logger.error("Invalid date format. It must respect one of the following formats: \"yyyy-MM-dd\" (e.g., \"2020-05-01\"), \"yyyy-MM-ddZ\" (e.g., \"2020-05-01+01:00\").");
			return;
		}

		logger.info("You are {} years old.", Long.valueOf(ChronoUnit.YEARS.between(birthday, LocalDate.now())));
	}

	/**
	 * @param arguments Parameters received from the command line.
	 * @return Whether the input array of arguments is valid.
	 */
	private static boolean isInputValid(final String[] arguments) {
		return ArrayUtils.getLength(arguments) == 1;
	}
}
