package com.nkunku.javaexercises.source00.stringnumbersandmath;

import java.util.Comparator;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which finds the character with the most appearances in the provided string.
 */
public final class MostAppearingCharacterFinder {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(MostAppearingCharacterFinder.class);

	/**
	 * @param arguments The arguments received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isValidInput(arguments)) {
			logger.warn("Invalid input: only one word must be provided.");
			return;
		}

		final Entry<Character, Long> characterWithMostAppearancesEntry = getCharacterWithMostAppearancesEntry(arguments[0]);
		if (characterWithMostAppearancesEntry != null) {
			logger.info("Character with the most appearances: \"{}\" ({} occurrences).",
					characterWithMostAppearancesEntry.getKey(), characterWithMostAppearancesEntry.getValue());
		} else {
			logger.info("No such character.");
		}
	}

	/**
	 * @param arguments The arguments received from the command line.
	 * @return Whether the input array of arguments is valid.
	 */
	private static boolean isValidInput(final String[] arguments) {
		return ArrayUtils.getLength(arguments) == 1;
	}

	/**
	 * @param word The word in which the character with the most appearances must be found.
	 * @return The character entry with the most appearances if it exists, {@code null} otherwise.
	 */
	private static Entry<Character, Long> getCharacterWithMostAppearancesEntry(final CharSequence word) {
		return word.chars().mapToObj(asciiId -> Character.valueOf((char) asciiId))
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
				.entrySet().stream().max(Comparator.comparingLong(Entry::getValue)).orElse(null);
	}
}
