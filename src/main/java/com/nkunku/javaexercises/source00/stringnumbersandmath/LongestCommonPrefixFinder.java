package com.nkunku.javaexercises.source00.stringnumbersandmath;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which finds the longest common prefix for the provided strings.
 */
public final class LongestCommonPrefixFinder {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(LongestCommonPrefixFinder.class);

	/**
	 * @param arguments Arguments received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isValidInput(arguments)) {
			logger.warn("Invalid input: at least 2 non-blank strings should be provided to find the longest common prefix.");
			return;
		}

		logger.info("Longest prefix for the words {}: {}.", Arrays.toString(arguments), getLongestCommonPrefix(arguments));
	}

	/**
	 * @param arguments Arguments received from the command line.
	 */
	private static boolean isValidInput(final String[] arguments) {
		return ArrayUtils.getLength(arguments) >= 2 && StringUtils.isNoneBlank(arguments);
	}

	/**
	 * @param arguments Arguments received from the command line.
	 */
	private static String getLongestCommonPrefix(final String[] arguments) {
		final String referenceWord = arguments[0];
		final int referenceWordLength = referenceWord.length();
		final StringBuilder stringBuilder = new StringBuilder(referenceWordLength);
		final String[] otherWords = ArrayUtils.subarray(arguments, 1, arguments.length);

		int charIdx = 0;
		boolean stillFindingLongestCommonPrefix = true;
		while (stillFindingLongestCommonPrefix && charIdx < referenceWordLength) {
			for (final String word : otherWords) {
				if (!word.startsWith(referenceWord.substring(0, charIdx + 1))) {
					stillFindingLongestCommonPrefix = false;
					break;
				}
			}
			if (stillFindingLongestCommonPrefix) {
				stringBuilder.append(referenceWord.charAt(charIdx++));
			}
		}

		return stringBuilder.length() == 0 ? null : stringBuilder.toString();
	}
}
