package com.nkunku.javaexercises.source00.stringnumbersandmath;

import java.util.Arrays;
import java.util.Comparator;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which sorts the strings in the array by ascending length.
 */
public final class StringArrayLengthSorter {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(StringArrayLengthSorter.class);

	/**
	 * @param arguments The arguments that have been received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isInputValid(arguments)) {
			logger.warn("Invalid input: at least 2 words must be provided for the strings to be sorted.");
			return;
		}

		logger.info("Sorted array of words: {}.", Arrays.toString(getStringArraySortedByAscendingLength(arguments)));
	}

	/**
	 * @param arguments The arguments that have been received from the command line.
	 * @return Whether the input array of arguments is valid.
	 */
	private static boolean isInputValid(final String[] arguments) {
		return ArrayUtils.getLength(arguments) >= 2;
	}

	/**
	 * @param words The array of words to be sorted.
	 * @return An array of contained the provided words which have been sorted by ascending length.
	 */
	private static String[] getStringArraySortedByAscendingLength(final String[] words) {
		return Arrays.stream(words).sorted(Comparator.comparingInt(String::length)).toArray(String[]::new);
	}
}
