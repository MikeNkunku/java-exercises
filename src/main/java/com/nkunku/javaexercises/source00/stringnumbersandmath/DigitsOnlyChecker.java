package com.nkunku.javaexercises.source00.stringnumbersandmath;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which checks that the provided string only contains digits.
 */
public final class DigitsOnlyChecker {

	/** Class regexp pattern. */
	private static final Pattern PATTERN = Pattern.compile("^(\\d+)$");

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(DigitsOnlyChecker.class);

	/**
	 * @param arguments The arguments received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isInputValid(arguments)) {
			logger.warn("Invalid input.");
			return;
		}

		final Matcher matcher = PATTERN.matcher(arguments[0]);
		logger.info(matcher.matches() ? "Match found: " + matcher.group(1) + '.' : "No match found.");
	}

	/**
	 * @param arguments The arguments received from the command line.
	 * @return Whether the input array of arguments is valid.
	 */
	private static boolean isInputValid(final String[] arguments) {
		return ArrayUtils.getLength(arguments) == 1;
	}
}
