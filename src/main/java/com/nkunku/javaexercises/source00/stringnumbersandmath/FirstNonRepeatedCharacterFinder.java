package com.nkunku.javaexercises.source00.stringnumbersandmath;

import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Executable class which finds the first non-repeated character in the provided string.
 */
public final class FirstNonRepeatedCharacterFinder {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(FirstNonRepeatedCharacterFinder.class);

	/**
	 * @param arguments Arguments received via command line.
	 */
	public static void main(final String[] arguments) {
		if (!isValidInput(arguments)) {
			logger.warn("Invalid input.");
			return;
		}

		logger.info("First non-repeated character: {}.", getFirstNonRepeatedCharacter(arguments[0]));
	}

	/**
	 * @param arguments Arguments received via command line.
	 * @return Whether the input array is valid.
	 */
	private static boolean isValidInput(final String[] arguments) {
		return ArrayUtils.getLength(arguments) == 1;
	}

	/**
	 * @param string The string in which the first non-repeated character must be found.
	 * @return The first non-repeated character which was found or {@code null} otherwise.
	 */
	private static Character getFirstNonRepeatedCharacter(final CharSequence string) {
		final LinkedHashMap<Character, Long> charactersOccurrencesMap = string.chars().mapToObj(asciiIdentifier -> Character.valueOf((char) asciiIdentifier))
				.collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()));

		return charactersOccurrencesMap.entrySet().stream().filter(entry -> entry.getValue().intValue() == 1).findFirst()
				.map(Entry::getKey).orElse(null);
	}
}
