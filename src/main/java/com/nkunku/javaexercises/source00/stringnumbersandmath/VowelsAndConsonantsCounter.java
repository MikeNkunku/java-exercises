package com.nkunku.javaexercises.source00.stringnumbersandmath;

import java.util.EnumMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which counts vowels and consonants which are contained in the provided string.
 */
public final class VowelsAndConsonantsCounter {

	/** Vowels in lowercase. */
	private static final char[] VOWELS = new char[] {'a', 'e', 'i', 'o', 'u'};

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(VowelsAndConsonantsCounter.class);

	/**
	 * @param arguments Arguments received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isValidInput(arguments)) {
			logger.warn("Invalid input.");
			return;
		}

		final EnumMap<LetterCategories, Long> letterCategoriesMap = getLetterCategoriesMap(arguments[0]);
		logger.info("Number of consonants: {}.\nNumber of vowels: {}.",
				letterCategoriesMap.get(LetterCategories.CONSONANTS), letterCategoriesMap.get(LetterCategories.VOWELS));
	}

	/**
	 * @param arguments Arguments received from the command line.
	 * @return Whether the input array of arguments is valid.
	 */
	private static boolean isValidInput(final String[] arguments) {
		return ArrayUtils.getLength(arguments) == 1;
	}

	/**
	 * @param word The word from which the consonants and vowels must be counted.
	 * @return An enum map containing the number of consonants and vowels in the provided word.
	 */
	private static EnumMap<LetterCategories, Long> getLetterCategoriesMap(final CharSequence word) {
		return word.chars().mapToObj(asciiIdentifier -> String.valueOf((char) asciiIdentifier).toLowerCase())
				.map(character -> ArrayUtils.contains(VOWELS, character.charAt(0)) ? LetterCategories.VOWELS : LetterCategories.CONSONANTS)
				.collect(Collectors.groupingBy(Function.identity(), () -> new EnumMap<>(LetterCategories.class),
						Collectors.counting()));
	}

	/**
	 * Enum representing the two categories of letters.
	 */
	private enum LetterCategories {
		/** Consonants. */
		CONSONANTS,
		/** Vowels. */
		VOWELS
	}
}
