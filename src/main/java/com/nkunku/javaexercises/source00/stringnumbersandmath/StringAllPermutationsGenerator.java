package com.nkunku.javaexercises.source00.stringnumbersandmath;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.IntStream;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which takes a string as input and generates all the possible permutations for it.
 */
public final class StringAllPermutationsGenerator {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(StringAllPermutationsGenerator.class);

	/**
	 * @param arguments Arguments received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isInputValid(arguments)) {
			logger.warn("Invalid input: only one argument must be passed.");
			return;
		}

		final Collection<String> wordPermutations = new ArrayList<>();
		collectAllPermutations("", arguments[0], wordPermutations);
		logger.info("All the permutations: {}.", wordPermutations);
	}

	/**
	 * @param arguments Arguments received from the command line.
	 * @return Whether the provided array of arguments is valid.
	 */
	private static boolean isInputValid(final String[] arguments) {
		return ArrayUtils.getLength(arguments) == 1;
	}

	/**
	 * @param prefix The prefix to use.
	 * @param word The original word.
	 * @param wordPermutations The collection to enrich with the generated permutations.
	 */
	private static void collectAllPermutations(final String prefix, final String word, final Collection<? super String> wordPermutations) {
		final int wordLength = word.length();
		if (wordLength == 0) {
			wordPermutations.add(prefix);
			return;
		}

		IntStream.range(0, wordLength)
				.forEach(charIdx -> collectAllPermutations(prefix + word.charAt(charIdx),
						word.substring(charIdx + 1, wordLength) + word.substring(0, charIdx), wordPermutations));
	}
}
