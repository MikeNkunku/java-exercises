package com.nkunku.javaexercises.source00.stringnumbersandmath;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class which can be run and displays in the console all duplicated characters in the provided string.
 */
public final class DuplicateCharactersCounter {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(DuplicateCharactersCounter.class);

	/**
	 * @param arguments The arguments which have been received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isInputValid(arguments)) {
			logger.warn("Invalid input.");
			return;
		}

		logger.info(getDuplicatedCharactersMessage(countCharactersOccurrences(arguments[0])));
	}

	/**
	 * @param arguments The array of arguments that has been received from the command line.
	 * @return Whether the array is valid for this class.
	 */
	private static boolean isInputValid(final String[] arguments) {
		return ArrayUtils.getLength(arguments) == 1;
	}

	/**
	 * @param string The string in which duplicates must be found.
	 * @return A map holding for the number of occurrences of each character.
	 */
	private static Map<Character, Long> countCharactersOccurrences(final CharSequence string) {
		return string.chars().mapToObj(asciiIdentifier -> Character.valueOf((char) asciiIdentifier))
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
	}

	/**
	 * @param charactersOccurrencesMap The map holding the number of occurrences for each character contained in the provided string.
	 * @return A message indicating the duplicated characters.
	 */
	private static String getDuplicatedCharactersMessage(final Map<Character, Long> charactersOccurrencesMap) {
		final StringBuilder stringBuilder = new StringBuilder("Duplicated characters:");
		charactersOccurrencesMap.entrySet().stream().filter(entry -> entry.getValue().intValue() > 1)
				.forEach(entry -> stringBuilder.append("\n\t").append(entry.getKey()).append(": ").append(entry.getValue()).append(" duplicates."));
		return stringBuilder.toString();
	}
}
