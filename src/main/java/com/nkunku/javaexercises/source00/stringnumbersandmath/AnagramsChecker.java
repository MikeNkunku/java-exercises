package com.nkunku.javaexercises.source00.stringnumbersandmath;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which checks whether the 2 provided strings are anagrams.<br>
 * White spaces are removed and the process is case-insensitive.
 */
public final class AnagramsChecker {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(AnagramsChecker.class);

	/**
	 * @param arguments The arguments that have been received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isInputValid(arguments)) {
			logger.warn("Invalid input: 2 words must be provided and neither can be blank.");
			return;
		}

		final String word1 = arguments[0];
		final String word2 = arguments[1];
		logger.info("Are \"{}\" and \"{}\" anagrams? {}.", word1, word2, Boolean.valueOf(areAnagrams(word1, word2)));
	}

	/**
	 * @param arguments The arguments that have been received from the command line.
	 * @return Whether the input array of arguments is valid.
	 */
	private static boolean isInputValid(final String[] arguments) {
		return ArrayUtils.getLength(arguments) == 2 && StringUtils.isNoneBlank(arguments);
	}

	/**
	 * @param word1 The first word to check.
	 * @param word2 The second word to check.
	 * @return Whether the provided words are anagrams of each other.
	 */
	private static boolean areAnagrams(final CharSequence word1, final CharSequence word2) {
		final Pattern removeWhitespacesPattern = Pattern.compile("\\s");
		final String whitespacelessWord1 = removeWhitespacesPattern.matcher(word1).replaceAll("");
		final String whitespacelessWord2 = removeWhitespacesPattern.matcher(word2).replaceAll("");
		if (whitespacelessWord1.length() != whitespacelessWord2.length()) {
			return false;
		}

		final String caseInsensitiveWord1 = whitespacelessWord1.toLowerCase();
		final String caseInsensitiveWord2 = whitespacelessWord2.toLowerCase();
		final Map<Character, Long> word1CharactersOccurrencesMap = getCharactersOccurrencesMap(caseInsensitiveWord1);
		final Map<Character, Long> word2CharactersOccurrencesMap = getCharactersOccurrencesMap(caseInsensitiveWord2);
		final Set<Character> word1KeySet = word1CharactersOccurrencesMap.keySet();
		final Set<Character> word2KeySet = word2CharactersOccurrencesMap.keySet();
		if (word1CharactersOccurrencesMap.size() != word2CharactersOccurrencesMap.size() || !word1KeySet.containsAll(word2KeySet)) {
			return false;
		}

		return word1KeySet.stream().allMatch(character -> word1CharactersOccurrencesMap.get(character).equals(word2CharactersOccurrencesMap.get(character)));
	}

	/**
	 * @param word The word whose characters' occurrences must be found.
	 * @return A map holding for each character its number of occurrences.
	 */
	private static Map<Character, Long> getCharactersOccurrencesMap(final CharSequence word) {
		return word.chars().mapToObj(asciiId -> Character.valueOf((char) asciiId)).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
	}
}
