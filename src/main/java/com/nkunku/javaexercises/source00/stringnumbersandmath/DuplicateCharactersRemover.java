package com.nkunku.javaexercises.source00.stringnumbersandmath;

import java.util.LinkedHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which removes all the duplicate characters present in the provided string.
 */
public final class DuplicateCharactersRemover {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(DuplicateCharactersRemover.class);

	/**
	 * @param arguments Arguments received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isValidInput(arguments)) {
			logger.warn("Invalid input: only one argument must be provided.");
			return;
		}

		logger.info("String without duplicate characters: {}.", getStringWithoutDuplicateCharacters(arguments[0]));
	}

	/**
	 * @param word The word which duplicates need to be removed from.
	 * @return The word without the duplicate characters.
	 */
	private static String getStringWithoutDuplicateCharacters(final CharSequence word) {
		return word.chars().mapToObj(asciiIdentifier -> Character.valueOf((char) asciiIdentifier))
				.collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()))
				.entrySet().stream().filter(entry -> entry.getValue().intValue() == 1)
				.map(entry -> String.valueOf(entry.getKey().charValue())).collect(Collectors.joining(""));
	}

	/**
	 * @param arguments Arguments received from the command line.
	 * @return Whether the input array of arguments is valid.
	 */
	private static boolean isValidInput(final String[] arguments) {
		return ArrayUtils.getLength(arguments) == 1;
	}

}
