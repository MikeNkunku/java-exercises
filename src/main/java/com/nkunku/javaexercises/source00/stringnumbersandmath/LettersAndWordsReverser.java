package com.nkunku.javaexercises.source00.stringnumbersandmath;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which reverses the words' order and words' characters.
 */
public final class LettersAndWordsReverser {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(LettersAndWordsReverser.class);

	/**
	 * @param arguments Arguments received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isInputValid(arguments)) {
			logger.warn("Invalid input.");
			return;
		}

		logger.info("Original words: {}.\nReversed words and letters: {}.", Arrays.toString(arguments),
				Arrays.toString(getReversedLettersAndWords(arguments)));
	}

	/**
	 * @param arguments Arguments received from the command line.
	 * @return Whether the input array of arguments is valid.
	 */
	private static boolean isInputValid(final String[] arguments) {
		return ArrayUtils.getLength(arguments) > 0;
	}

	/**
	 * @param words The words whose letters and order need to be reversed.
	 * @return A string array containing the reversed words in the reverse order.
	 */
	private static String[] getReversedLettersAndWords(final String[] words) {
		final String[] reversedWords = Arrays.copyOf(words, words.length);
		ArrayUtils.reverse(reversedWords);
		return Arrays.stream(reversedWords).map(word -> new StringBuilder(word).reverse().toString()).toArray(String[]::new);
	}
}
