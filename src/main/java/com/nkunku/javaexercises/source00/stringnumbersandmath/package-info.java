/**
 * Package dedicated to exercises found in the <b>String, Numbers and Math</b> part of the <b>Java Coding Problems</b> book.
 */
package com.nkunku.javaexercises.source00.stringnumbersandmath;
