package com.nkunku.javaexercises.source00.stringnumbersandmath;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which counts the number of occurrences of the specified character in the provided string.
 */
public final class CharacterOccurrencesInStringCounter {

	/** Class logger. */
	private static final Logger logger = LogManager.getLogger(CharacterOccurrencesInStringCounter.class);

	/**
	 * @param arguments The arguments received from the command line.
	 */
	public static void main(final String[] arguments) {
		if (!isInputValid(arguments)) {
			logger.warn("Invalid input: there must be 2 arguments and the first one must be a single character.");
			return;
		}

		final String characterToCount = arguments[0];
		final long nbOccurrences = arguments[1].chars().mapToObj(asciiIdentifier -> String.valueOf((char) asciiIdentifier))
				.filter(characterToCount::equals).count();
		logger.info("The '{}' character has been found {} time(s).", characterToCount, Long.valueOf(nbOccurrences));
	}

	/**
	 * @param arguments The arguments received from the command line.
	 * @return Whether the input array of arguments is valid: for this statement to be true, there must be 2 arguments and the first one must
	 * be a single character.
	 */
	private static boolean isInputValid(final String[] arguments) {
		return ArrayUtils.getLength(arguments) == 2 && arguments[0].length() == 1;
	}
}
