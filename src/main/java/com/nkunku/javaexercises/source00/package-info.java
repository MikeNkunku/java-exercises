/**
 * Package containing all the exercises contained in the <b>Java Coding Problems</b> book.
 */
package com.nkunku.javaexercises.source00;
