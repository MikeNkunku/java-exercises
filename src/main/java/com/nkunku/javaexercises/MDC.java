package com.nkunku.javaexercises;

import java.time.Duration;

import org.apache.logging.log4j.ThreadContext;

public enum MDC {
	DATE("date"),
	EXECUTION_TIME("executionTime"),
	MISCELLANEOUS("misc"),
	USER_EMAIL("user.email"),
	USER_NAME("user.name");

	private final String id;

	MDC(final String anId) {
		id = anId;
	}

	public String id() {
		return id;
	}

	public static void logExecutionTime(final Duration duration) {
		ThreadContext.put(EXECUTION_TIME.id, duration.toMillis() + " ms");
	}

}
