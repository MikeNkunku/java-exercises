package com.nkunku.javaexercises.source01.sortingalgorithms;

public class BubbleSort implements SortingAlgorithm {

	private static final String NAME = "Bubble Sort";

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void sort(final long[] arrayToSort) {
		final int arrayLength = arrayToSort.length;
		boolean swapPerformed;
		long tmp;
		do {
			swapPerformed = false;
			for (int i = 0; i < arrayLength - 1; i++) {
				if (arrayToSort[i] > arrayToSort[i + 1]) {
					swapPerformed = true;
					tmp = arrayToSort[i + 1];
					arrayToSort[i + 1] = arrayToSort[i];
					arrayToSort[i] = tmp;
				}
			}
		} while (swapPerformed);
	}
}
