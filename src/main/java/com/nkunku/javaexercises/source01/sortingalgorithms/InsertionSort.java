package com.nkunku.javaexercises.source01.sortingalgorithms;

public class InsertionSort implements SortingAlgorithm {

	private static final String NAME = "Insertion Sort";

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void sort(final long[] arrayToSort) {
		final int arrayLength = arrayToSort.length;
		long current;
		int j;
		for (int i = 1; i < arrayLength; i++) {
			current = arrayToSort[i];
			j = i - 1;
			// We move elements from the left to the right as long as the value under test is greater than the current value
			while (j >= 0 && arrayToSort[j] > current) {
				arrayToSort[j + 1] = arrayToSort[j--];
			}
			// Then we insert the current value before all the values which are bigger
			arrayToSort[j + 1] = current;
		}
	}
}
