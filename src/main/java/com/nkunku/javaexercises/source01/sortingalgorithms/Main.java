package com.nkunku.javaexercises.source01.sortingalgorithms;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Runnable class which sorts the provided array using the <b>bubble sort</b> algorithm.
 */
public final class Main {

	private static final long MIN_NB_ELEMENTS = 3L;

	private static final Logger logger = LogManager.getLogger(Main.class);

	public static void main(final String[] args) {
		if (ArrayUtils.getLength(args) < MIN_NB_ELEMENTS) {
			logger.warn("Invalid number of arguments: at least {} elements must be provided.", Long.valueOf(MIN_NB_ELEMENTS));
			return;
		}

		final SortingAlgorithm sortingAlgorithm;
		try {
			sortingAlgorithm = SortingAlgorithmFactory.get(args[0]);
		} catch (final IllegalArgumentException illegalArgumentException) {
			logger.error(illegalArgumentException);
			return;
		}

		final long[] arrayToSort;
		try {
			arrayToSort = getNumericArrayFromStringArray(args);
		} catch (final NumberFormatException e) {
			logger.error("At least one of the numbers provided could not be converted into a long value.", e);
			return;
		}

		final long[] originalArray = Arrays.copyOf(arrayToSort, arrayToSort.length);
		final Instant startInstant = Instant.now();
		sortingAlgorithm.sort(arrayToSort);
		final long executionTimeNanos = Duration.between(startInstant, Instant.now()).toNanos();
		logger.info("Array to sort: {}.\nSorted array using the {} algorithm: {}.\nSorting execution time: {} ns.",
				Arrays.toString(originalArray), sortingAlgorithm.getName(), Arrays.toString(arrayToSort), Long.valueOf(executionTimeNanos));
	}

	private static long[] getNumericArrayFromStringArray(final String[] args) {
		final long[] output = new long[args.length - 1];
		for (int i = 1; i < args.length; i++) {
			output[i - 1] = Long.parseLong(args[i]);
		}
		return output;
	}
}
