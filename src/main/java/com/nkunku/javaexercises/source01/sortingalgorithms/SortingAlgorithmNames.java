package com.nkunku.javaexercises.source01.sortingalgorithms;

public enum SortingAlgorithmNames {
	BUBBLE_SORT("bubbleSort"),
	INSERTION_SORT("insertionSort"),
	SELECTION_SORT("selectionSort");

	private final String key;

	SortingAlgorithmNames(final String keyValue) {
		key = keyValue;
	}

	public static SortingAlgorithmNames fromKey(final String keyValue) {
		for (final SortingAlgorithmNames sortingAlgorithmName : SortingAlgorithmNames.values()) {
			if (sortingAlgorithmName.key.equals(keyValue)) {
				return sortingAlgorithmName;
			}
		}
		throw new IllegalArgumentException("No valid value found for the \"" + keyValue + "\" key value");
	}
}
