package com.nkunku.javaexercises.source01.sortingalgorithms;

import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;

public final class SortingAlgorithmFactory {

	private static final Map<SortingAlgorithmNames, SortingAlgorithm> algorithmsMap = buildAlgorithmsMap();

	public static SortingAlgorithm get(final String sortingAlgorithmKey) {
		return algorithmsMap.get(SortingAlgorithmNames.fromKey(sortingAlgorithmKey));
	}

	private static Map<SortingAlgorithmNames, SortingAlgorithm> buildAlgorithmsMap() {
		final Map<SortingAlgorithmNames, SortingAlgorithm> sortingAlgorithmNamesMap = new EnumMap<>(SortingAlgorithmNames.class);
		sortingAlgorithmNamesMap.put(SortingAlgorithmNames.BUBBLE_SORT, new BubbleSort());
		sortingAlgorithmNamesMap.put(SortingAlgorithmNames.INSERTION_SORT, new InsertionSort());
		sortingAlgorithmNamesMap.put(SortingAlgorithmNames.SELECTION_SORT, new SelectionSort());
		return Collections.unmodifiableMap(sortingAlgorithmNamesMap);
	}
}
