package com.nkunku.javaexercises.source01.sortingalgorithms;

public class SelectionSort implements SortingAlgorithm {

	private static final String NAME = "Selection Sort";

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void sort(final long[] arrayToSort) {
		final int arrayLength = arrayToSort.length;
		for (int i = 0; i < arrayLength; i++) {
			final long currentValue = arrayToSort[i];
			long minValue = currentValue;
			int minValueIdx = i;
			// We look for the minimum value starting from the "i"th position
			for (int j = i + 1; j < arrayLength; j++) {
				if (arrayToSort[j] < minValue) {
					minValue = arrayToSort[j];
					minValueIdx = j;
				}
			}
			// And then, we swap places of the current and minimum values in the array
			arrayToSort[i] = minValue;
			arrayToSort[minValueIdx] = currentValue;
		}
	}
}
