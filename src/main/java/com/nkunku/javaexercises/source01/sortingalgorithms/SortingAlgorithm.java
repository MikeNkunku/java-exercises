package com.nkunku.javaexercises.source01.sortingalgorithms;

public interface SortingAlgorithm {

	String getName();
	void sort(long[] arrayToSort);
}
