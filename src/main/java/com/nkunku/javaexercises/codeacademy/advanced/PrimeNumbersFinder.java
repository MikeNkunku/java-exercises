package com.nkunku.javaexercises.codeacademy.advanced;

import java.util.Arrays;

final class PrimeNumbersFinder {

    private PrimeNumbersFinder() {}

    static int[] find(final int[] numbers) {
        return Arrays.stream(numbers).filter(PrimeNumbersFinder::isPrime).toArray();
    }

    private static boolean isPrime(final int number) {
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

}
