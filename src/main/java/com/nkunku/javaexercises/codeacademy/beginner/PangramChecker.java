package com.nkunku.javaexercises.codeacademy.beginner;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

final class PangramChecker {

    private static final int ALPHABET_LENGTH = 26;

    private PangramChecker() {}

    static boolean check(final String sentence) {
        final Map<String, Long> charactersMap = Arrays.stream(sentence.replaceAll("[^a-zA-Z]", "")
                        .toLowerCase().split(""))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return charactersMap.size() == ALPHABET_LENGTH;
    }
}
