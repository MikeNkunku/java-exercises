package com.nkunku.javaexercises.w3resource.basicexercisespartone;

final class SameRightmostDigitChecker {

	private SameRightmostDigitChecker() {}

	/**
	 * @param firstNumber The first number to check.
	 * @param secondNumber The second number to check.
	 * @param thirdNumber The third number to check.
	 * @return {@code true} if two or more numbers have the same rightmost digit; {@code false} otherwise.
	 */
	static boolean check(final int firstNumber, final int secondNumber, final int thirdNumber) {
		if (firstNumber < 0 || secondNumber < 0 || thirdNumber < 0) {
			throw new IllegalArgumentException("Only positive numbers are allowed");
		}

		final int firstNumberRightmostDigit = firstNumber % 10;
		final int secondNumberRightmostDigit = secondNumber % 10;
		final int thirdNumberRightmostDigit = thirdNumber % 10;

		return firstNumberRightmostDigit == secondNumberRightmostDigit
				|| firstNumberRightmostDigit == thirdNumberRightmostDigit
				|| secondNumberRightmostDigit == thirdNumberRightmostDigit;
	}
}
