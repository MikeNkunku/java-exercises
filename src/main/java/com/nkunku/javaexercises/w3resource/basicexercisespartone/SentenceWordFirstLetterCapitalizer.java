package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

final class SentenceWordFirstLetterCapitalizer {

	private SentenceWordFirstLetterCapitalizer() {}

	static String capitalize(final String sentence) {
		if (StringUtils.isEmpty(sentence)) {
			return sentence;
		}

		return Arrays.stream(sentence.split(" ")).map(SentenceWordFirstLetterCapitalizer::capitalizeWord)
				.collect(Collectors.joining(" "));
	}

	private static String capitalizeWord(final String word) {
		if (StringUtils.length(word) == 1) {
			return word.toUpperCase();
		}

		return String.valueOf(word.charAt(0)).toUpperCase() + word.substring(1);
	}
}
