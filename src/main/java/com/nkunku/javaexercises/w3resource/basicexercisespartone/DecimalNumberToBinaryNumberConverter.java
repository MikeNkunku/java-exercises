package com.nkunku.javaexercises.w3resource.basicexercisespartone;

final class DecimalNumberToBinaryNumberConverter {

	private DecimalNumberToBinaryNumberConverter() {}

	static int convert(final int decimalNumber) {
		return Integer.parseInt(Integer.toBinaryString(decimalNumber));
	}
}
