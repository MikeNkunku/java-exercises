package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import java.nio.file.Files;
import java.nio.file.Path;

final class FileSizeGetter {

	private FileSizeGetter() {}

	static long get(final String filePathStr) throws Exception {
		return Files.size(Path.of(filePathStr));
	}
}
