package com.nkunku.javaexercises.w3resource.basicexercisespartone;

final class DigitSumOperator {

	private DigitSumOperator() {}

	static long sum(final long number) {
		if (number < 0L) {
			throw new IllegalArgumentException("A positive number must be provided");
		}

		long sum = 0L;
		for (final String str : String.valueOf(number).split("")) {
			sum += Long.parseLong(str);
		}
		return sum;
	}
}
