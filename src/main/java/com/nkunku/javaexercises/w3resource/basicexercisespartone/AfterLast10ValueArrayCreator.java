package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import java.util.Arrays;

final class AfterLast10ValueArrayCreator {

	private AfterLast10ValueArrayCreator() {}

	static int[] create(final int[] numbers) {
		int lastIndexOf10 = -1;
		final int nbNumbers = numbers.length;
		for (int i = 0; i < nbNumbers; i++) {
			if (numbers[i] == 10) {
				lastIndexOf10 = i;
			}
		}

		return lastIndexOf10 == -1 || lastIndexOf10 + 1 == nbNumbers ? new int[]{}
				: Arrays.copyOfRange(numbers, lastIndexOf10 + 1, nbNumbers);
	}
}
