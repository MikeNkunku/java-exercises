package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

final class EvenAndOddNumbersCounter {

	private EvenAndOddNumbersCounter() {}

	static Map<Categories, Long> count(final int[] numbers) {
		return Arrays.stream(numbers).boxed()
				.collect(Collectors.groupingBy(Categories::getCategory, Collectors.counting()));
	}

	enum Categories {
		EVEN(integer -> Boolean.valueOf(integer.intValue() % 2 == 0)),
		ODD(integer -> Boolean.valueOf(integer.intValue() % 2 == 1));

		private final Function<Integer, Boolean> func;

		Categories(final Function<Integer, Boolean> aFunction) {
			func = aFunction;
		}

		static Categories getCategory(final Integer integer) {
			return Arrays.stream(Categories.values()).filter(cat -> cat.func.apply(integer).booleanValue())
					.findFirst().get();
		}
	}
}
