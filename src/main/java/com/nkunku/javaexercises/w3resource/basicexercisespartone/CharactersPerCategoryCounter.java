package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.regex.Pattern;

final class CharactersPerCategoryCounter {

	private CharactersPerCategoryCounter() {}

	static Map<Categories, Long> count(final String text) {
		final Map<Categories, Long> map = new EnumMap<>(Categories.class);

		for (final String character: text.split("")) {
			final Categories cat = Arrays.stream(Categories.values()).filter(c -> c.matches(character))
					.findFirst().orElse(Categories.MISCELLANEOUS);
			final Long value = map.getOrDefault(cat, Long.valueOf(0L));
			map.put(cat, Long.valueOf(value.longValue() + 1L));
		}

		return map;
	}

	enum Categories {
		LETTERS(Pattern.compile("[A-Za-z]")),
		NUMBERS(Pattern.compile("\\d")),
		SPACES(Pattern.compile("\\s")),
		MISCELLANEOUS(Pattern.compile("[^A-Za-z\\d\\s]"));

		private final Pattern pattern;

		Categories(final Pattern aRegexpPattern) {
			pattern = aRegexpPattern;
		}

		boolean matches(final String character) {
			return pattern.matcher(character).matches();
		}
	}
}
