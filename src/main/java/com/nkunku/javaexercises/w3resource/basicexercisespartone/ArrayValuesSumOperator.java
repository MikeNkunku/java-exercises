package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import java.util.Arrays;

final class ArrayValuesSumOperator {

	private ArrayValuesSumOperator() {}

	static long sum(final int[] numbers) {
		return Arrays.stream(numbers).sum();
	}
}
