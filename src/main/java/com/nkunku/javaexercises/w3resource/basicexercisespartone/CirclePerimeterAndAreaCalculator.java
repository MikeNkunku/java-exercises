package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import java.util.HashMap;
import java.util.Map;

final class CirclePerimeterAndAreaCalculator {

	private CirclePerimeterAndAreaCalculator() {}

	static Map<String, Double> calculate(final double radius) {
		final Map<String, Double> map = new HashMap<>(2);
		map.put(Keys.PERIMETER.id(), Double.valueOf(2.0d * radius * Math.PI));
		map.put(Keys.AREA.id(), Double.valueOf(radius * radius * Math.PI));
		return map;
	}

	enum Keys {
		AREA("area"),
		PERIMETER("perimeter");

		private final String id;

		Keys(final String anId) {
			id = anId;
		}

		public String id() {
			return id;
		}
	}
}
