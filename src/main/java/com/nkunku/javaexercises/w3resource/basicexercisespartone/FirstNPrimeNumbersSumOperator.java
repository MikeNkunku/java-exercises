package com.nkunku.javaexercises.w3resource.basicexercisespartone;

final class FirstNPrimeNumbersSumOperator {

	private FirstNPrimeNumbersSumOperator() {}

	static long sum(final long nbPrimeNumbers) {
		long primeSum = 0L;
		long nbAddedPrimeNumbers = 0L;
		long currentNumber = 2L;

		while (nbAddedPrimeNumbers < nbPrimeNumbers) {
			if (isPrime(currentNumber)) {
				primeSum += currentNumber;
				++nbAddedPrimeNumbers;
			}
			++currentNumber;
		}

		return primeSum;
	}

	private static boolean isPrime(final long number) {
		for (long i = 2L; (double) i < Math.sqrt((double) number); i++) {
			if (number % i == 0L) {
				return false;
			}
		}
		return true;
	}
}
