package com.nkunku.javaexercises.w3resource.arrays;

import java.util.*;

import org.apache.commons.lang3.ArrayUtils;

final class LargestContiguousSumInSubArrayFinder {

    private LargestContiguousSumInSubArrayFinder() {}

    static int[] find(final int[] numbers) {
        final List<int[]> allSubArrays = getAllContiguousSubArrays(numbers);

        return findSmallestSubArrayWithLargestSum(allSubArrays);
    }

    private static List<int[]> getAllContiguousSubArrays(final int[] numbers) {
        final List<int[]> allSubArrays = new ArrayList<>();

        final int nbNumbers = numbers.length;
        for (int arraySize = 2; arraySize <= nbNumbers; arraySize++) {
            for (int startIdx = 0; startIdx <= nbNumbers - arraySize; startIdx++) {
                allSubArrays.add(ArrayUtils.subarray(numbers, startIdx, startIdx + arraySize));
            }
        }

        return allSubArrays;
    }

    private static int[] findSmallestSubArrayWithLargestSum(final List<int[]> allSubArrays) {
        int largestSum = Integer.MIN_VALUE;
        int[] subArrayWithLargestSum = {largestSum};

        for (int[] subArr : allSubArrays) {
            final int sum = Arrays.stream(subArr).sum();
            if (largestSum < sum || (sum == largestSum && subArr.length < subArrayWithLargestSum.length)) {
                largestSum = sum;
                subArrayWithLargestSum = subArr;
            }
        }

        return subArrayWithLargestSum;
    }
}
