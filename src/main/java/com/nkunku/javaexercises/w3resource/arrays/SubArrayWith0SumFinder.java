package com.nkunku.javaexercises.w3resource.arrays;

import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;

final class SubArrayWith0SumFinder {

	private SubArrayWith0SumFinder() {}

	static List<int[]> find(final int[] numbers) {
		final List<int[]> allSubArrays = new ArrayList<>();
		final int nbNumbers = numbers.length;
		for (int targetSize = 2; targetSize <= nbNumbers; targetSize++) {
			for (int eltToAddIdx = 0; eltToAddIdx < nbNumbers - targetSize; eltToAddIdx++) {
				addAllSubArrays(numbers, targetSize, eltToAddIdx, new int[]{}, allSubArrays);
			}
		}

		return allSubArrays.stream().filter(subArray -> Arrays.stream(subArray).sum() == 0)
				.collect(Collectors.toList());
	}

	private static void addAllSubArrays(final int[] numbers, final int targetSize, final int eltToAddIdx,
										final int[] dataToAdd, final List<int[]> allSubArrays) {
		if (targetSize <= dataToAdd.length) {
			if (allSubArrays.stream().noneMatch(subArr -> Objects.deepEquals(dataToAdd, subArr))) {
				allSubArrays.add(dataToAdd);
			}
			return;
		}

		for (int i = eltToAddIdx + 1; i < numbers.length; i++) {
			addAllSubArrays(numbers, targetSize, i, ArrayUtils.add(dataToAdd, numbers[eltToAddIdx]), allSubArrays);
		}
	}
}
