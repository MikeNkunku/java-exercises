package com.nkunku.javaexercises.w3resource.arrays;

final class SubArrayWith0SumChecker {

	private SubArrayWith0SumChecker() {}

	static boolean check(final int[] numbers) {
		for (int i = 0; i < numbers.length; i++) {
			long sum = numbers[i];
			for (int j = i + 1; j < numbers.length; j++) {
				sum += numbers[j];
				if (sum == 0) {
					return true;
				}
			}
		}

		return false;
	}
}
