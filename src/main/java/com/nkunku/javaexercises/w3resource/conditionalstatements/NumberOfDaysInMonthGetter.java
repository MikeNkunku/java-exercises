package com.nkunku.javaexercises.w3resource.conditionalstatements;

import java.time.YearMonth;

final class NumberOfDaysInMonthGetter {

    private NumberOfDaysInMonthGetter() {}

    static int get(final int year, final int monthId) {
        return YearMonth.of(year, monthId).atEndOfMonth().getDayOfMonth();
    }
}
