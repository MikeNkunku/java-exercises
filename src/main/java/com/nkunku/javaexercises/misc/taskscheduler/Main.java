package com.nkunku.javaexercises.misc.taskscheduler;

import java.util.ArrayList;
import java.util.List;

import com.nkunku.javaexercises.misc.taskscheduler.domain.Priorities;
import com.nkunku.javaexercises.misc.taskscheduler.domain.Task;
import com.nkunku.javaexercises.misc.taskscheduler.domain.TaskScheduler;

public final class Main {

	public static void main(final String[] args) {
		final TaskScheduler taskScheduler = new TaskScheduler(buildTasks());
		taskScheduler.runAllTasks();
	}

	private static List<Task> buildTasks() {
		final List<Task> tasks = new ArrayList<>(5);
		tasks.add(new Task("ID01", Priorities.LOW));
		tasks.add(new Task("ID02", Priorities.HIGH));
		tasks.add(new Task("ID03", Priorities.LOW));
		tasks.add(new Task("ID04", Priorities.MEDIUM));
		tasks.add(new Task("ID05", Priorities.HIGH));
		return tasks;
	}
}
