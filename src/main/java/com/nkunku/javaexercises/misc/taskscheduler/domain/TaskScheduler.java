package com.nkunku.javaexercises.misc.taskscheduler.domain;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TaskScheduler {
	private static final Comparator<Task> TASK_PRIORITY_COMPARATOR = Comparator.comparingInt(task -> task.getPriority().value());

	private static final Logger logger = LogManager.getLogger(TaskScheduler.class);

	private final List<Task> scheduledTasks;

	public TaskScheduler(final List<Task> tasksToSchedule) {
		scheduledTasks = Objects.requireNonNull(tasksToSchedule, "The provided collection of tasks cannot be null");
		scheduledTasks.sort(TASK_PRIORITY_COMPARATOR);
	}

	public void runAllTasks() {
		while (!scheduledTasks.isEmpty()) {
			runNextTask();
		}
	}

	public void runNextTask() {
		if (scheduledTasks.isEmpty()) {
			throw new IllegalStateException("There's no task to process");
		}

		final Task task = scheduledTasks.get(0);
		task.run();
		scheduledTasks.remove(task);
		logger.info("Finished processing task with ID \"{}\".", task.getId());
	}
}
