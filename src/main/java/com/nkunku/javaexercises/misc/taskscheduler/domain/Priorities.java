package com.nkunku.javaexercises.misc.taskscheduler.domain;

/**
 * The lower the priority value is, the more important it is.
 */
public enum Priorities {
	LOW(2), HIGH(0), MEDIUM(1);

	private final int value;

	Priorities(final int val) {
		value = val;
	}

	public int value() {
		return value;
	}
}
