package com.nkunku.javaexercises.misc.taskscheduler.domain;

import java.util.Objects;

public class Task {
	private final String id;
	private final Priorities priority;

	public Task(final String idValue, final Priorities priorityValue) {
		id = Objects.requireNonNull(idValue, "The provided ID cannot be null");
		priority = Objects.requireNonNull(priorityValue, "The provided priority cannot be null");
	}

	public void run() {
		// Do nothing
	}

	public String getId() {
		return id;
	}

	public Priorities getPriority() {
		return priority;
	}
}
