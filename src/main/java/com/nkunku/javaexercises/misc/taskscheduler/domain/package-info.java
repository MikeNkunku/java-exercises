/**
 * Package regrouping entities which are used by the task scheduler.
 */
package com.nkunku.javaexercises.misc.taskscheduler.domain;
