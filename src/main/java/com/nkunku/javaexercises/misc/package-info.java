/**
 * Package containing exercises which come from miscellaneous sources.
 */
package com.nkunku.javaexercises.misc;
