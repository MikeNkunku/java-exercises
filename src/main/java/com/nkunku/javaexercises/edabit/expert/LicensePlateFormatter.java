package com.nkunku.javaexercises.edabit.expert;

final class LicensePlateFormatter {

    private LicensePlateFormatter() {}

    static String format(final String licensePlate, final int groupSize) {
        final String uppercaseLicensePlate = licensePlate.replaceAll("-", "").toUpperCase();
        final StringBuilder strBuilder = new StringBuilder();
        completeNewLicensePlate(uppercaseLicensePlate, 0, groupSize, strBuilder);

        return strBuilder.toString();
    }

    private static void completeNewLicensePlate(final String uppercaseLicensePlate, final int startIdx,
                                                final int groupSize, final StringBuilder strBuilder) {
        if (uppercaseLicensePlate.length() -1 <= startIdx) {
            return;
        }

        final int endExclusiveIdx = startIdx == 0 && uppercaseLicensePlate.length() % groupSize != 0 ?
                uppercaseLicensePlate.length() % groupSize : startIdx + groupSize;
        strBuilder.append(startIdx != 0 ? '-' : "").append(uppercaseLicensePlate, startIdx, endExclusiveIdx);
        completeNewLicensePlate(uppercaseLicensePlate, endExclusiveIdx, groupSize, strBuilder);
    }
}
