package com.nkunku.javaexercises.edabit.expert;

final class ConsecutiveAscendingNumbersChecker {

    private ConsecutiveAscendingNumbersChecker() {}

    static boolean check(final String numberSequence) {
        final int sequenceLength = numberSequence.length();
        if (sequenceLength < 2) {
            return false;
        }

        final int maxGroupSize =  sequenceLength / 2;
        for (int groupSize = 1; groupSize <= maxGroupSize; groupSize++) {
            if (sequenceLength % groupSize != 0) {
                continue;
            }

            final int nbNumbers = sequenceLength / groupSize;
            final int[] numbers = new int[nbNumbers];
            for (int i = 0; i < nbNumbers; i++) {
                numbers[i] = Integer.parseInt(numberSequence.substring(i * groupSize, i * groupSize + groupSize));
                if (1 <= i && numbers[i] - numbers[i - 1] != 1) {
                    break;
                }
                if (i == nbNumbers - 1) {
                    return true;
                }
            }
        }

        return false;
    }
}
