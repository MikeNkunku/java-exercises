package com.nkunku.javaexercises.edabit.expert;

import java.util.*;

import org.apache.commons.lang3.StringUtils;

final class NLengthLetterGroupsProducer {

    private NLengthLetterGroupsProducer() {}

    static String[] produce(final String word, final int nbLetters) {
        final int wordLength = StringUtils.length(word);
        if (wordLength < nbLetters) {
            return new String[0];
        }

        final List<String> groups = new ArrayList<>(wordLength / nbLetters);
        addGroups(word, nbLetters, groups);
        groups.sort(Comparator.naturalOrder());

        return groups.toArray(String[]::new);
    }

    private static void addGroups(final String word, final int nbLetters, final List<String> groups) {
        if (StringUtils.length(word) < nbLetters) {
            return;
        }

        groups.add(word.substring(0, nbLetters));
        addGroups(word.substring(nbLetters), nbLetters, groups);
    }
}
