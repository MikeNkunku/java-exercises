package com.nkunku.javaexercises.edabit.expert;

import java.util.ArrayList;
import java.util.List;

final class FibonacciWordSequenceGenerator {

    private static final int MIN_NB_FIRST_ELEMENTS = 2;
    static final String INVALID_MSG = "invalid";

    private FibonacciWordSequenceGenerator() {}

    static String generate(final int nbFirstElements) {
        if (nbFirstElements < MIN_NB_FIRST_ELEMENTS) {
            return INVALID_MSG;
        }

        final List<String> fibonacciWords = new ArrayList<>(nbFirstElements);
        addWords(MIN_NB_FIRST_ELEMENTS, nbFirstElements, fibonacciWords);

        return String.join(", ", fibonacciWords);
    }

    private static void addWords(final int nbElements, final int nbFirstElements, final List<String> fibonacciWords) {
        if (nbFirstElements < nbElements) {
            return;
        }

        if (nbElements == MIN_NB_FIRST_ELEMENTS) {
            fibonacciWords.add("b");
            fibonacciWords.add("a");
        } else {
            fibonacciWords.add(fibonacciWords.get(nbElements - MIN_NB_FIRST_ELEMENTS)
                    + fibonacciWords.get(nbElements - MIN_NB_FIRST_ELEMENTS - 1));
        }

        addWords(nbElements + 1, nbFirstElements, fibonacciWords);
    }
}
