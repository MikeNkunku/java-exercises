package com.nkunku.javaexercises.edabit.expert;

import java.util.Arrays;
import java.util.stream.IntStream;

final class PrisonerExecutor {

    private static final int EXECUTED_INDICATOR = -1;

    private PrisonerExecutor() {}

    static int whoGoesFree(final int nbPrisoners, final int stepSize) {
        final int[] prisoners = IntStream.range(0, nbPrisoners).toArray();
        int idx = -1;
        int nbSteps = 0;
        while (Arrays.stream(prisoners).filter(p -> p != EXECUTED_INDICATOR).count() != 1L) {
            if (prisoners[++idx % nbPrisoners] != EXECUTED_INDICATOR) {
                ++nbSteps;
            }
            if (nbSteps == stepSize) {
                prisoners[idx % nbPrisoners] = EXECUTED_INDICATOR;
                nbSteps = 0;
            }
        }

        return Arrays.stream(prisoners).filter(p -> p != EXECUTED_INDICATOR).findFirst().getAsInt();
    }
}
