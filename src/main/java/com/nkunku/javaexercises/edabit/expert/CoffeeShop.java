package com.nkunku.javaexercises.edabit.expert;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nkunku.javaexercises.edabit.expert.CoffeeShop.MenuItem.Types;

final class CoffeeShop {

    private static final Logger logger = LogManager.getLogger(CoffeeShop.class);

    static final String UNAVAILABLE_ITEM_MSG = "This item is currently unavailable!";
    static final String ORDER_ADDED_MSG = "Order added!";
    static final String ALL_ORDERS_FULFILLED_MSG = "All orders have been fulfilled!";

    static class MenuItem {

        enum Types {
            FOOD, DRINK
        }

        private final String name;
        private final Types type;
        private final double price;

        MenuItem(final String aName, final Types aType, final double aPrice) {
            name = aName;
            type = aType;
            price = aPrice;
        }

        String getName() {
            return name;
        }

        double getPrice() {
            return price;
        }
    }

    private final String name;
    private final MenuItem[] menu;
    private final Queue<String> orders;
    private double dueAmount;
    private String cheapestItemName;
    private String[] drinkNamesOnly = null;
    private String[] foodNamesOnly = null;

    CoffeeShop(final String aName, final MenuItem[] menuItems) {
        name = aName;
        menu = menuItems;
        orders = new LinkedList<>();
        dueAmount = 0d;
        final Optional<MenuItem> cheapestItem = Arrays.stream(menu).min(Comparator.comparingDouble(MenuItem::getPrice));
        cheapestItem.ifPresent(menuItem -> cheapestItemName = menuItem.name);
    }

    public void addOrder(final String item) {
        final Optional<MenuItem> menuItem = Arrays.stream(menu).filter(mi -> mi.name.equals(item)).findFirst();
        if (menuItem.isEmpty()) {
            if (logger.isInfoEnabled()) {
                logger.info(UNAVAILABLE_ITEM_MSG);
            }
        } else {
            orders.add(item);
            if (logger.isInfoEnabled()) {
                logger.info(ORDER_ADDED_MSG);
            }
            dueAmount += menuItem.get().price;
        }
    }

    public void fulfillOrder() {
        if (orders.isEmpty()) {
            if (logger.isInfoEnabled()) {
                logger.info(ALL_ORDERS_FULFILLED_MSG);
            }
            return;
        }

        final String itemName = orders.poll();
        if (logger.isInfoEnabled()) {
            logger.info("The {} is ready!", itemName);
        }
        if (orders.isEmpty()) {
            dueAmount = 0d;
        }
    }

    public String[] listOrders() {
        return orders.toArray(String[]::new);
    }

    public double getDueAmount() {
        return dueAmount;
    }

    public String getCheapestItemName() {
        return cheapestItemName;
    }

    public String[] getDrinkNamesOnly() {
        if (drinkNamesOnly == null) {
            drinkNamesOnly = Arrays.stream(menu).filter(item -> Types.DRINK.equals(item.type))
                    .map(MenuItem::getName).toArray(String[]::new);
        }
        return drinkNamesOnly;
    }

    public String[] getFoodNamesOnly() {
        if (foodNamesOnly == null) {
            foodNamesOnly = Arrays.stream(menu).filter(item -> Types.FOOD.equals(item.type))
                    .map(MenuItem::getName).toArray(String[]::new);
        }
        return foodNamesOnly;
    }
}
