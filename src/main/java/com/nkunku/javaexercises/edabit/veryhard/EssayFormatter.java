package com.nkunku.javaexercises.edabit.veryhard;

final class EssayFormatter {

    private EssayFormatter() {}

    static String format(final int nbWords, final int lineMaxLength, final String essay) {
        int nbProcessedWords = 0;
        final String[] words = essay.split(" ");

        int currentLineLength = 0;
        final StringBuilder stringBuilder = new StringBuilder();
        while (nbProcessedWords < nbWords) {
            String word = words[nbProcessedWords++];
            boolean firstWordOnLine = currentLineLength == 0;
            if (lineMaxLength < currentLineLength + word.length()) {
                stringBuilder.append('\n');
                firstWordOnLine = true;
                currentLineLength = 0;
            }
            stringBuilder.append(firstWordOnLine ? "" : ' ').append(word);
            currentLineLength += word.length();
        }
        return stringBuilder.toString();
    }
}
