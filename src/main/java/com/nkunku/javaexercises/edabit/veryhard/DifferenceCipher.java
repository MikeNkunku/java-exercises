package com.nkunku.javaexercises.edabit.veryhard;

/**
 * <a href="https://edabit.com/challenge/KZrmicjc8zCZyGNee">Challenge page</a>.
 */
final class DifferenceCipher {

    private DifferenceCipher() {}

    static int[] encrypt(final String message) {
        final int msgLength = message.length();
        final int[] encryptedMsg = new int[msgLength];
        final char[] characters = message.toCharArray();
        for (int i = 0; i < msgLength; i++) {
            encryptedMsg[i] = i == 0 ? (int) characters[i] : (int) characters[i] - (int) characters[i - 1];
        }
        return encryptedMsg;
    }

    static String decrypt(final int[] codedMessage) {
        final int msgLength = codedMessage.length;
        final StringBuilder msgBuilder = new StringBuilder(msgLength);
        int currentAsciiCode = codedMessage[0];
        for (int i = 0; i < msgLength; i++) {
            if (i != 0) {
                currentAsciiCode += codedMessage[i];
            }
            msgBuilder.append((char) currentAsciiCode);
        }
        return msgBuilder.toString();
    }
}
