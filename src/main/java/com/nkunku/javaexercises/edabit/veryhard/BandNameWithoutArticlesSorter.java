package com.nkunku.javaexercises.edabit.veryhard;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Function;

/**
 * <a href="https://edabit.com/challenge/iaKJJoxdk5GNDqzJC">Challenge page</a>.
 */
final class BandNameWithoutArticlesSorter {

    private static final Function<String, String> BAND_NAME_WITHOUT_ARTICLE_FUNCTION = bandName ->
            bandName.replaceAll("^[Tt]he\\s", "").replaceAll("^[Aa]n?\\s", "");
    private BandNameWithoutArticlesSorter() {}

    static void sort(final String[] bandNames) {
        Arrays.sort(bandNames, Comparator.comparing(BAND_NAME_WITHOUT_ARTICLE_FUNCTION));
    }
}
