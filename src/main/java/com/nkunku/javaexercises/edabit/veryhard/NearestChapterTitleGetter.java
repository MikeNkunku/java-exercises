package com.nkunku.javaexercises.edabit.veryhard;

/**
 * <a href="https://edabit.com/challenge/WtxvjezcFYGQo2v48">Challenge page</a>.
 */
final class NearestChapterTitleGetter {

    private NearestChapterTitleGetter() {}

    static String get(final Chapter[] chapters, final int currentPage) {
        int pageDiff = Integer.MAX_VALUE;
        int nearestChapterStartPage = Integer.MAX_VALUE;

        String nearestChapterTitle = "";
        for (Chapter chapter : chapters) {
            int diff = Math.abs(currentPage - chapter.startPage);
            if (diff < pageDiff || pageDiff == diff && nearestChapterStartPage < chapter.startPage) {
                pageDiff = diff;
                nearestChapterStartPage = chapter.startPage;
                nearestChapterTitle = chapter.title;
            }
        }

        return nearestChapterTitle;
    }

    static class Chapter {
        private final String title;
        private final int startPage;

        Chapter(final String aTitle, final int startPageValue) {
            title = aTitle;
            startPage = startPageValue;
        }
    }
}
