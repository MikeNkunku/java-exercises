package com.nkunku.javaexercises.edabit.veryhard;

import java.util.Arrays;
import java.util.stream.IntStream;

final class IncreasingDecreasingNumbersCounter {

    private IncreasingDecreasingNumbersCounter() {}

    static int count(final int powerOfTen) {
        return (int) IntStream.range(1, (int) (StrictMath.pow(10, powerOfTen) + 1))
                .filter(number -> IncreasingDecreasingNumbersCounter.isIncreasing(number)
                        || IncreasingDecreasingNumbersCounter.isDecreasing(number))
                .count();
    }

    private static boolean isIncreasing(final int number) {
        final int[] digits = Arrays.stream(String.valueOf(number).split("")).mapToInt(Integer::parseInt)
                .toArray();
        int previousVal = digits[0];
        for (int i = 1; i < digits.length; i++) {
            if (digits[i] - previousVal < 0) {
                return false;
            }
            previousVal = digits[i];
        }
        return true;
    }

    private static boolean isDecreasing(final int number) {
        final int[] digits = Arrays.stream(String.valueOf(number).split("")).mapToInt(Integer::parseInt)
                .toArray();
        int previousVal = digits[0];
        for (int i = 1; i < digits.length; i++) {
            if (0 < digits[i] - previousVal) {
                return false;
            }
            previousVal = digits[i];
        }
        return true;
    }
}
