package com.nkunku.javaexercises.edabit.veryhard;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

final class PalindromicAnagramChecker {

    private PalindromicAnagramChecker() {}

    static boolean check(final String word) {
        final Map<String, Long> lettersMap = Arrays.stream(word.split(""))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        return canBeAPalindromeWithAnEvenNumberOfLetters(word, lettersMap)
                || canBeAPalindromeWithAnOddNumberOfLetters(word, lettersMap);
    }

    private static boolean canBeAPalindromeWithAnEvenNumberOfLetters(final String word,
                                                                     final Map<String, Long> lettersMap) {
        return word.length() % 2 == 0 && lettersMap.values().stream()
                .noneMatch(counter -> counter.longValue() % 2 != 0);
    }

    private static boolean canBeAPalindromeWithAnOddNumberOfLetters(final String word,
                                                                    final Map<String, Long> lettersMap) {
        long nbOddLetters = lettersMap.values().stream().filter(count -> count.longValue() % 2 == 1).count();

        return word.length() % 2 == 1 && nbOddLetters == 1L;
    }
}
