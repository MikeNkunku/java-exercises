package com.nkunku.javaexercises.edabit.hard;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Function;

final class ObjectArraySorter {

    private static final Function<Object, Integer> OBJECT_TO_INTEGER_FUNCTION = obj -> obj instanceof Integer
            ? (Integer) obj : Integer.valueOf(((int[]) obj)[0]);

    private ObjectArraySorter() {}

    static Object[] getSortedArray(final Object[] array) {
        final Object[] arr = Arrays.copyOf(array, array.length);
        Arrays.sort(arr, Comparator.comparing(OBJECT_TO_INTEGER_FUNCTION));

        return arr;
    }
}
