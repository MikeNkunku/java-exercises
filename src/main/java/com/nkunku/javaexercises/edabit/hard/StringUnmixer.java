package com.nkunku.javaexercises.edabit.hard;

import org.apache.commons.lang3.StringUtils;

/**
 * <a href="https://edabit.com/challenge/XRAGxXj4KtakkvD3F">Challenge page</a>.
 */
final class StringUnmixer {

    private StringUnmixer() {}

    static String unmix(final String sentence) {
        final int sLength = StringUtils.length(sentence);
        if (sLength <= 1) {
            return sentence;
        }

        final StringBuilder stringBuilder = new StringBuilder(sLength);
        final boolean lengthEven = sLength % 2 == 0;
        final int limit = lengthEven ? sLength : sLength - 1;
        for (int i = 0; i < limit; i += 2) {
            stringBuilder.append(sentence.charAt(i + 1)).append(sentence.charAt(i));
        }
        if (!lengthEven) {
            stringBuilder.append(sentence.charAt(sLength - 1));
        }

        return stringBuilder.toString();
    }
}
