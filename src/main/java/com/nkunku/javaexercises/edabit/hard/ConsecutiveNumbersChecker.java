package com.nkunku.javaexercises.edabit.hard;

import java.util.Arrays;

final class ConsecutiveNumbersChecker {

    private ConsecutiveNumbersChecker() {}

    static boolean check(final int[] numbers) {
        Arrays.sort(numbers);
        for (int idx = 0; idx < numbers.length - 1; idx++) {
            if (numbers[idx + 1] - numbers[idx] != 1) {
                return false;
            }
        }
        return true;
    }
}
