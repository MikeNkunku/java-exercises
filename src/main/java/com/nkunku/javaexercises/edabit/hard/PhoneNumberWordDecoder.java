package com.nkunku.javaexercises.edabit.hard;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

final class PhoneNumberWordDecoder {

    private PhoneNumberWordDecoder() {}

    static String decode(final String phoneNumberWithWords) {
        String output = phoneNumberWithWords;
        for (Entry<String, String> entry : getRegexMap().entrySet()) {
            output = output.replaceAll(entry.getKey(), entry.getValue());
        }
        return output;
    }

    private static Map<String, String> getRegexMap() {
        final Map<String, String> map = new HashMap<>(8);

        map.put("[ABC]", "2");
        map.put("[DEF]", "3");
        map.put("[GHI]", "4");
        map.put("[JKL]", "5");
        map.put("[MNO]", "6");
        map.put("[PQRS]", "7");
        map.put("[TUV]", "8");
        map.put("[WXYZ]", "9");

        return map;
    }
}
