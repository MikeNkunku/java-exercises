package com.nkunku.javaexercises.edabit.hard;

import java.util.function.IntBinaryOperator;
import java.util.stream.IntStream;

final class DisariumNumberChecker {

    private DisariumNumberChecker() {}

    static boolean check(final int number) {
        final String[] digits = String.valueOf(number).split("");
        final IntBinaryOperator disariumSumOperator = (acc, idx) -> acc
                + (int) StrictMath.pow(Integer.parseInt(digits[idx - 1]), idx);

        return IntStream.range(1, digits.length + 1).reduce(0, disariumSumOperator) == number;
    }
}
