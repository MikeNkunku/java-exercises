package com.nkunku.javaexercises.edabit.hard;

import java.util.Arrays;

final class SevenBoomOperator {

    static final String SEVEN_FOUND_MESSAGE = "Boom!";
    static final String SEVEN_NOT_FOUND_MESSAGE = "There is no 7 in the array";

    private SevenBoomOperator() {}

    static String operate(final int[] numbers) {
        return Arrays.stream(numbers).anyMatch(number -> String.valueOf(number).contains("7")) ? SEVEN_FOUND_MESSAGE
                : SEVEN_NOT_FOUND_MESSAGE;
    }
}
