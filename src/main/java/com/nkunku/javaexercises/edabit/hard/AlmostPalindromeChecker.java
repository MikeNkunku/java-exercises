package com.nkunku.javaexercises.edabit.hard;

/**
 * <a href="https://edabit.com/challenge/E9AHcnWg7fDfardLF">Challenge page</a>.
 */
final class AlmostPalindromeChecker {

    private AlmostPalindromeChecker() {}

    static boolean check(final String string) {
        int nbCharactersToChange = 0;

        final int stringLength = string.length();
        for (int i = 0; i < stringLength / 2; i++) {
            if (string.charAt(i) != string.charAt(stringLength - 1 - i)) {
                ++nbCharactersToChange;
            }
        }

        return nbCharactersToChange == 1;
    }
}
