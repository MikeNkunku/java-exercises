package com.nkunku.javaexercises.edabit.hard;

final class FactorialChecker {

    private FactorialChecker() {}

    static boolean check(final int number) {
        int factorial = 1;
        int i = 2;
        while (factorial < number) {
            factorial *= i++;
        }

        return factorial == number;
    }
}
