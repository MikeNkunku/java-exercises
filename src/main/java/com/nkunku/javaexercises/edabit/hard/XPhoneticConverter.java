package com.nkunku.javaexercises.edabit.hard;

/**
 * <a href="https://edabit.com/challenge/g5n85W62rJqZakMry">Challenge page</a>.
 */
final class XPhoneticConverter {

    private XPhoneticConverter() {}

    static String convert(final String sentence) {
        return sentence.replaceAll("\\bX\\b", "Ecks")
                .replaceAll("\\bx\\b", "ecks")
                .replaceAll("\\bX(\\w+)", "Z$1")
                .replaceAll("\\bx(\\w+)", "z$1")
                .replaceAll("x", "cks");
    }
}
