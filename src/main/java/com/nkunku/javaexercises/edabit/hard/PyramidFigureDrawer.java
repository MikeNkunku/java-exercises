package com.nkunku.javaexercises.edabit.hard;

/**
 * <a href="https://edabit.com/challenge/qaMkkFAaTJzbmkwwe">Challenge</a>.
 */
final class PyramidFigureDrawer {

    private static final int MIN_NB_LINES = 2;

    private PyramidFigureDrawer() {}

    static String draw(final int nbLines) {
        if (nbLines < MIN_NB_LINES) {
            throw new IllegalArgumentException("Only positive integers equal or greater than 2 are supported");
        }

        final StringBuilder strBuilder = new StringBuilder();
        for (int lineIdx = 0; lineIdx < nbLines; lineIdx++) {
            drawLeftPart(nbLines, lineIdx, strBuilder);
            drawStars(lineIdx, strBuilder);
            drawRightPart(nbLines, lineIdx, strBuilder);
            strBuilder.append('\n');
        }

        return strBuilder.toString();
    }

    private static void drawLeftPart(final int nbLines, final int lineIdx, final StringBuilder strBuilder) {
        if (nbLines - 1 == lineIdx) {
            return;
        }

        strBuilder.append("/".repeat(4 * (nbLines - lineIdx - 1)));
    }

    private static void drawStars(final int lineIdx, final StringBuilder strBuilder) {
        if (lineIdx == 0) {
            return;
        }

        strBuilder.append("*".repeat( 8 * lineIdx));
    }

    private static void drawRightPart(final int nbLines, final int lineIdx, final StringBuilder strBuilder) {
        if (nbLines - 1 == lineIdx) {
            return;
        }

        strBuilder.append("\\".repeat(4 * (nbLines - lineIdx - 1)));
    }
}
