package com.nkunku.javaexercises.edabit.hard;

import java.util.Arrays;

/**
 * <a href="https://edabit.com/challenge/Nwq8yK3Eq2DqbxDsL">Challenge</a>.
 */
final class OuterLayerOffPeeler {

    private OuterLayerOffPeeler() {}

    static Object[][] peelOff(final Object[][] arrays) {
        if (arrays.length <= 2) {
            return new Object[0][];
        }

        final Object[][] output = new Object[arrays.length - 2][];
        for (int i = 1; i <= arrays.length - 2; i++) {
            final Object[] array = arrays[i];
            output[i - 1] = Arrays.copyOfRange(array, 1, array.length - 1);
        }

        return output;
    }
}
