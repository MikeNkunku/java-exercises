package com.nkunku.javaexercises.edabit.hard;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

/**
 * <a href="https://edabit.com/challenge/EjQM5woRAhYEmuGFp">Challenge page</a>.
 */
final class RemainingElementsProductPartitioner {

    private RemainingElementsProductPartitioner() {}

    static boolean canPartition(final int[] numbers) {
        if (2 <= ArrayUtils.indexesOf(numbers, 0).cardinality()) {
            return true;
        }

        Arrays.sort(numbers);
        return canPartitionWithMinValue(numbers) || canPartitionWithMaxValue(numbers);
    }

    private static boolean canPartitionWithMinValue(final int[] sortedNumbers) {
        return Arrays.stream(ArrayUtils.subarray(sortedNumbers, 1, sortedNumbers.length))
                .reduce(1, Math::multiplyExact) == sortedNumbers[0];
    }

    private static boolean canPartitionWithMaxValue(final int[] sortedNumbers) {
        return Arrays.stream(ArrayUtils.subarray(sortedNumbers, 0, sortedNumbers.length - 1))
                .reduce(1, Math::multiplyExact) == sortedNumbers[sortedNumbers.length - 1];
    }
}
