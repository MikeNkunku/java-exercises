package com.nkunku.javaexercises.edabit.hard;

import java.util.stream.IntStream;

final class NumberOfPrimesGetter {

    private NumberOfPrimesGetter() {}

    static int get(final int number) {
        return (int) IntStream.range(2, number + 1).filter(NumberOfPrimesGetter::isPrime).count();
    }

    private static boolean isPrime(final int n) {
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }
}
