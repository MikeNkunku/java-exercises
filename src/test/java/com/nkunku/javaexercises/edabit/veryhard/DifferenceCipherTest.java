package com.nkunku.javaexercises.edabit.veryhard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DifferenceCipherTest {

    @Test
    void decrypt_useCase1() {
        final int[] codedMessage = {84, 20, -3, -69, 78, -9, 4, -2, 1, -6, 13, 6, -3, 1, -83, 65, 17, -13, -69, 83, 1,
                -2, -17, 13, -7, -2, -55, 0};
        assertEquals("The neighbours are strange..", DifferenceCipher.decrypt(codedMessage));
    }

    @Test
    void decrypt_useCase2() {
        assertEquals("OK", DifferenceCipher.decrypt(new int[]{ 79, -4 }));
    }

    @Test
    void decrypt_useCase3() {
        final int[] codedMessage = {84, 27, -2, 2, 3, 0, -3, 8, -75, -12, 19, -19, 80, -3, -77, 73, 5, -78, 84, -12, -3,
                -69, 71, -6, 17, -14, 1, 9, -64};
        assertEquals("Tomorrow, 3 pm in the garden.", DifferenceCipher.decrypt(codedMessage));
    }

    @Test
    void encrypt_questionMark() {
        assertArrayEquals(new int[]{ 63 }, DifferenceCipher.encrypt("?"));
    }

    @Test
    void encrypt_useCase2() {
        final int[] expectedEncodedMessage = {73, 43, -77, 76, -83, 65, -65, 83, -14, -2, 15, -13, 15, -83};
        assertArrayEquals(expectedEncodedMessage, DifferenceCipher.encrypt("It's a secret!"));
    }

    @Test
    void encrypt_useCase3() {
        assertArrayEquals(new int[]{83, 34, -7, 5, -11, 1, 5, -9}, DifferenceCipher.encrypt("Sunshine"));
    }
}
