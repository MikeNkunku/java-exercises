package com.nkunku.javaexercises.edabit.veryhard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IncreasingDecreasingNumbersCounterTest {

    @Test
    void count_useCase01() {
        assertEquals(1, IncreasingDecreasingNumbersCounter.count(0));
    }

    @Test
    void count_useCase02() {
        assertEquals(10, IncreasingDecreasingNumbersCounter.count(1));
    }

    @Test
    void count_useCase03() {
        assertEquals(100, IncreasingDecreasingNumbersCounter.count(2));
    }

    @Test
    void count_useCase04() {
        assertEquals(475, IncreasingDecreasingNumbersCounter.count(3));
    }

    @Test
    void count_useCase05() {
        assertEquals(1675, IncreasingDecreasingNumbersCounter.count(4));
    }

    @Test
    void count_useCase06() {
        assertEquals(4954, IncreasingDecreasingNumbersCounter.count(5));
    }

    @Test
    void count_useCase07() {
        assertEquals(12952, IncreasingDecreasingNumbersCounter.count(6));
    }
}
