package com.nkunku.javaexercises.edabit.veryhard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PalindromicAnagramCheckerTest {

    @Test
    void check_useCase01() {
        assertTrue(PalindromicAnagramChecker.check("rearcac"));
    }

    @Test
    void check_useCase02() {
        assertTrue(PalindromicAnagramChecker.check("suhbeusheff"));
    }

    @Test
    void check_useCase03() {
        assertFalse(PalindromicAnagramChecker.check("palindrome"));
    }

    @Test
    void check_useCase04() {
        assertFalse(PalindromicAnagramChecker.check("yagnx"));
    }

    @Test
    void check_useCase05() {
        assertFalse(PalindromicAnagramChecker.check("zgzqxljjp"));
    }

    @Test
    void check_useCase06() {
        assertFalse(PalindromicAnagramChecker.check("tgmqkpdhnhatoco"));
    }

    @Test
    void check_useCase07() {
        assertTrue(PalindromicAnagramChecker.check("akyka"));
    }

    @Test
    void check_useCase08() {
        assertFalse(PalindromicAnagramChecker.check("kjyyrftnbsbq"));
    }

    @Test
    void check_useCase09() {
        assertFalse(PalindromicAnagramChecker.check("jynmynqhcy"));
    }

    @Test
    void check_useCase10() {
        assertFalse(PalindromicAnagramChecker.check("hfe"));
    }

    @Test
    void check_useCase11() {
        assertTrue(PalindromicAnagramChecker.check("noon"));
    }

    @Test
    void check_useCase12() {
        assertFalse(PalindromicAnagramChecker.check("azmkallbanpu"));
    }

    @Test
    void check_useCase13() {
        assertTrue(PalindromicAnagramChecker.check("drrede"));
    }

    @Test
    void check_useCase14() {
        assertFalse(PalindromicAnagramChecker.check("xmhwcocldjdnqvv"));
    }

    @Test
    void check_useCase15() {
        assertTrue(PalindromicAnagramChecker.check("reparpe"));
    }

    @Test
    void check_useCase16() {
        assertFalse(PalindromicAnagramChecker.check("jnavz"));
    }

    @Test
    void check_useCase17() {
        assertTrue(PalindromicAnagramChecker.check("orort"));
    }

    @Test
    void check_useCase18() {
        assertFalse(PalindromicAnagramChecker.check("mel"));
    }

    @Test
    void check_useCase19() {
        assertFalse(PalindromicAnagramChecker.check("jdxknf"));
    }

    @Test
    void check_useCase20() {
        assertFalse(PalindromicAnagramChecker.check("qo"));
    }

    @Test
    void check_useCase21() {
        assertTrue(PalindromicAnagramChecker.check("neett"));
    }

    @Test
    void check_useCase22() {
        assertTrue(PalindromicAnagramChecker.check("wow"));
    }

    @Test
    void check_useCase23() {
        assertTrue(PalindromicAnagramChecker.check("avkkiaapiusuapspiip"));
    }

    @Test
    void check_useCase24() {
        assertTrue(PalindromicAnagramChecker.check("aann"));
    }

    @Test
    void check_useCase25() {
        assertTrue(PalindromicAnagramChecker.check("iivcc"));
    }

    @Test
    void check_useCase26() {
        assertTrue(PalindromicAnagramChecker.check("akyka"));
    }

    @Test
    void check_useCase27() {
        assertTrue(PalindromicAnagramChecker.check("eelvl"));
    }

    @Test
    void check_useCase28() {
        assertTrue(PalindromicAnagramChecker.check("damam"));
    }

    @Test
    void check_useCase29() {
        assertTrue(PalindromicAnagramChecker.check("mmo"));
    }

    @Test
    void check_useCase30() {
        assertFalse(PalindromicAnagramChecker.check("ge"));
    }

    @Test
    void check_useCase31() {
        assertTrue(PalindromicAnagramChecker.check("arrad"));
    }

    @Test
    void check_useCase32() {
        assertFalse(PalindromicAnagramChecker.check("bq"));
    }

    @Test
    void check_useCase33() {
        assertFalse(PalindromicAnagramChecker.check("djufyllynldw"));
    }

    @Test
    void check_useCase34() {
        assertTrue(PalindromicAnagramChecker.check("reparpe"));
    }

    @Test
    void check_useCase35() {
        assertTrue(PalindromicAnagramChecker.check("ttraoor"));
    }

    @Test
    void check_useCase36() {
        assertTrue(PalindromicAnagramChecker.check("orort"));
    }

    @Test
    void check_useCase37() {
        assertTrue(PalindromicAnagramChecker.check("asgas"));
    }

    @Test
    void check_useCase38() {
        assertTrue(PalindromicAnagramChecker.check("t"));
    }

    @Test
    void check_useCase39() {
        assertTrue(PalindromicAnagramChecker.check("tstsa"));
    }

    @Test
    void check_useCase40() {
        assertTrue(PalindromicAnagramChecker.check("neett"));
    }

    @Test
    void check_useCase41() {
        assertFalse(PalindromicAnagramChecker.check("wndnwrkpkihup"));
    }
}
