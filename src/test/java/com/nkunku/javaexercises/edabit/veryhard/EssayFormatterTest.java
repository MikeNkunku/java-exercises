package com.nkunku.javaexercises.edabit.veryhard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EssayFormatterTest {

    @Test
    void test1() {
        assertEquals("hello my\nname is\nBessie\nand this\nis my\nessay",
                EssayFormatter.format(10,7,"hello my name is Bessie and this is my essay"));
    }

    @Test
    void test2() {
        assertEquals("college students\nin the USA are\neligible for\nselection as\nfinalists",
                EssayFormatter.format(11,16,"college students in the USA are eligible for "
                        + "selection as finalists"));
    }
}
