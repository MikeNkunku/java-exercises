package com.nkunku.javaexercises.edabit.veryhard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BandNameWithoutArticlesSorterTest {

    @Test
    void sort_useCase01() {
        final String[] bandNames = new String[] {"The Plot in You", "The Devil Wears Prada", "Pierce the Veil",
                "Norma Jean", "The Bled", "Say Anything", "The Midway State", "We Came as Romans", "Counterparts",
                "Oh, Sleeper", "A Skylit Drive", "Anywhere But Here", "An Old Dog"};
        final String[] expectedSortedBandNames = new String[] {"Anywhere But Here", "The Bled", "Counterparts",
                "The Devil Wears Prada", "The Midway State", "Norma Jean", "Oh, Sleeper", "An Old Dog",
                "Pierce the Veil", "The Plot in You", "Say Anything", "A Skylit Drive", "We Came as Romans"};

        BandNameWithoutArticlesSorter.sort(bandNames);

        assertArrayEquals(expectedSortedBandNames, bandNames);
    }

    @Test
    void sort_useCase02() {
        final String[] bandNames = new String[] {"The New Yardbirds", "The Beatles", "The Square Roots", "On A Friday",
                "An Irony"};
        final String[] expectedSortedBandNames = new String[] {"The Beatles", "An Irony", "The New Yardbirds",
                "On A Friday", "The Square Roots"};

        BandNameWithoutArticlesSorter.sort(bandNames);

        assertArrayEquals(expectedSortedBandNames, bandNames);
    }

    @Test
    void sort_useCase03() {
        final String[] bandNames = new String[] {"The Platters", "A Yard of Yarn", "The Everly Brothers",
                "A Monster Effect", "The Sex Maggots"};
        final String[] expectedSortedBandNames = new String[] {"The Everly Brothers", "A Monster Effect",
                "The Platters", "The Sex Maggots", "A Yard of Yarn"};

        BandNameWithoutArticlesSorter.sort(bandNames);

        assertArrayEquals(expectedSortedBandNames, bandNames);
    }

    @Test
    void sort_useCase04() {
        final String[] bandNames = new String[] {"But Myth", "An Old Dog", "Def Leppard", "Thereafter",
                "The Any Glitters", "The Dawn"};
        final String[] expectedSortedBandNames = new String[] {"The Any Glitters", "But Myth", "The Dawn",
                "Def Leppard", "An Old Dog", "Thereafter"};

        BandNameWithoutArticlesSorter.sort(bandNames);

        assertArrayEquals(expectedSortedBandNames, bandNames);
    }

    @Test
    void sort_useCase05() {
        final String[] bandNames = {"Van Halen", "Analytics", "The Beastie Boys", "Radiohead", "The Gamma Ray",
                "Led Zeppelin"};
        final String[] expectedSortedBandNames = {"Analytics", "The Beastie Boys", "The Gamma Ray", "Led Zeppelin",
                "Radiohead", "Van Halen"};

        BandNameWithoutArticlesSorter.sort(bandNames);

        assertArrayEquals(expectedSortedBandNames, bandNames);
    }

    @Test
    void sort_useCase06() {
        final String[] bandNames = new String[] {"The Young Guns", "The Fugees", "Coldplay", "The Grateful Dead",
                "The Artistics"};
        final String[] expectedSortedBandNames = new String[] {"The Artistics", "Coldplay", "The Fugees",
                "The Grateful Dead", "The Young Guns"};

        BandNameWithoutArticlesSorter.sort(bandNames);

        assertArrayEquals(expectedSortedBandNames, bandNames);
    }

    @Test
    void sort_useCase07() {
        final String[] bandNames = new String[] {"The Old American Rejects", "The Cure", "The Who", "Goo Goo Dolls",
                "The Bee Gees"};
        final String[] expectedSortedBandNames = new String[] {"The Bee Gees", "The Cure", "Goo Goo Dolls",
                "The Old American Rejects", "The Who"};

        BandNameWithoutArticlesSorter.sort(bandNames);

        assertArrayEquals(expectedSortedBandNames, bandNames);
    }

}
