package com.nkunku.javaexercises.edabit.veryhard;

import org.junit.jupiter.api.Test;

import com.nkunku.javaexercises.edabit.veryhard.NearestChapterTitleGetter.Chapter;

import static org.junit.jupiter.api.Assertions.*;

class NearestChapterTitleGetterTest {

    @Test
    void get_useCase01() {
        assertEquals("Chapter 2", NearestChapterTitleGetter.get(new Chapter[] {
                new Chapter("Chapter 1", 1), new Chapter("Chapter 2", 15),
                new Chapter("Chapter 3", 37)}, 10));
    }

    @Test
    void get_useCase02() {
        assertEquals("The End?", NearestChapterTitleGetter.get(new Chapter[] {
                new Chapter("New Beginnings", 1), new Chapter("Strange Developments", 62),
                new Chapter("The End?", 194), new Chapter("The True Ending", 460)},
                200));
    }

    @Test
    void get_useCase03() {
        assertEquals("Chapter 1b", NearestChapterTitleGetter.get(new Chapter[] {new Chapter("Chapter 1a", 1),
                new Chapter("Chapter 1b", 5)}, 3));
    }

    @Test
    void get_useCase04() {
        assertEquals("Chapter 1d", NearestChapterTitleGetter.get(new Chapter[] {
                new Chapter("Chapter 1a", 1), new Chapter("Chapter 1b", 5),
                new Chapter("Chapter 1c", 50), new Chapter("Chapter 1d", 100)}, 75));
    }

    @Test
    void get_useCase05() {
        assertEquals("Chapter 1e", NearestChapterTitleGetter.get(new Chapter[] {
                new Chapter("Chapter 1a", 1), new Chapter("Chapter 1b", 5),
                new Chapter("Chapter 1c", 50), new Chapter("Chapter 1d", 100),
                new Chapter("Chapter 1e", 200)}, 150));
    }

    @Test
    void get_useCase06() {
        assertEquals("Chapter 1c", NearestChapterTitleGetter.get(new Chapter[] {
                new Chapter("Chapter 1a", 1), new Chapter("Chapter 1b", 5),
                new Chapter("Chapter 1c", 50), new Chapter("Chapter 1d", 100),
                new Chapter("Chapter 1e", 200)}, 74));
    }

    @Test
    void get_useCase07() {
        assertEquals("Chapter 1f", NearestChapterTitleGetter.get(new Chapter[] {
                new Chapter("Chapter 1a", 1), new Chapter("Chapter 1b", 5),
                new Chapter("Chapter 1c", 50), new Chapter("Chapter 1d", 100),
                new Chapter("Chapter 1e", 200), new Chapter("Chapter 1f", 400)}, 300));
    }

    @Test
    void get_useCase08() {
        assertEquals("Chapter Five", NearestChapterTitleGetter.get(new Chapter[] {
                new Chapter("Chapter Four", 46), new Chapter("Chapter Five", 54)}, 50));
    }
}
