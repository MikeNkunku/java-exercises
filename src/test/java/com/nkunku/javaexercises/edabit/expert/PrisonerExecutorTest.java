package com.nkunku.javaexercises.edabit.expert;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrisonerExecutorTest {

    @Test
    void whoGoesFree_9PrisonersAndStepSize2_2() {
        assertEquals(2, PrisonerExecutor.whoGoesFree(9, 2));
    }

    @Test
    void whoGoesFree_9PrisonersAndStepSize3_0() {
        assertEquals(0, PrisonerExecutor.whoGoesFree(9, 3));
    }

    @Test
    void whoGoesFree_7PrisonersAndStepSize2_6() {
        assertEquals(6, PrisonerExecutor.whoGoesFree(7, 2));
    }

    @Test
    void whoGoesFree_7PrisonersAndStepSize3_3() {
        assertEquals(3, PrisonerExecutor.whoGoesFree(7, 3));
    }

    @Test
    void whoGoesFree_15PrisonersAndStepSize4_12() {
        assertEquals(12, PrisonerExecutor.whoGoesFree(15, 4));
    }

    @Test
    void whoGoesFree_14PrisonersAndStepSize3_1() {
        assertEquals(1, PrisonerExecutor.whoGoesFree(14, 3));
    }

    @Test
    void whoGoesFree_53PrisonersAndStepSize7_21() {
        assertEquals(21, PrisonerExecutor.whoGoesFree(53, 7));
    }

    @Test
    void whoGoesFree_543PrisonersAndStepSize21_455() {
        assertEquals(455, PrisonerExecutor.whoGoesFree(543, 21));
    }

    @Test
    void whoGoesFree_673PrisonersAndStepSize13_303() {
        assertEquals(303, PrisonerExecutor.whoGoesFree(673, 13));
    }
}
