package com.nkunku.javaexercises.edabit.expert;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LicensePlateFormatterTest {

    @Test
    void format_useCase01() {
        assertEquals("5F3Z-2E9W", LicensePlateFormatter.format("5F3Z-2e-9-w", 4));
    }

    @Test
    void format_useCase02() {
        assertEquals("2-5G-3J", LicensePlateFormatter.format("2-5g-3-J", 2));
    }

    @Test
    void format_useCase03() {
        assertEquals("24-A0R-74K", LicensePlateFormatter.format("2-4A0r7-4k", 3));
    }

    @Test
    void format_useCase04() {
        assertEquals("GB-BD-51-9K-FC", LicensePlateFormatter.format("GB-bd519-KFC", 2));
    }

    @Test
    void format_useCase05() {
        assertEquals("BF-11G-F9I-781-9IT", LicensePlateFormatter.format("BF-11gf9i-7819iT", 3));
    }

    @Test
    void format_useCase06() {
        assertEquals("FIN-MMG-418", LicensePlateFormatter.format("Fin-Mmg-418", 3));
    }

    @Test
    void format_useCase07() {
        assertEquals("S-PXO-755", LicensePlateFormatter.format("sPx-o755", 3));
    }

    @Test
    void format_useCase08() {
        assertEquals("DE-57-UK", LicensePlateFormatter.format("de57-uk", 2));
    }

    @Test
    void format_useCase09() {
        assertEquals("D-KAPA-7778", LicensePlateFormatter.format("d-kapa-7778", 4));
    }

    @Test
    void format_useCase10() {
        assertEquals("NL-J20-6FV", LicensePlateFormatter.format("nlj-206-fv", 3));
    }
}
