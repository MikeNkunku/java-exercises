package com.nkunku.javaexercises.edabit.expert;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConsecutiveAscendingNumbersCheckerTest {

    @Test
    void check_232425_true() {
        assertTrue(ConsecutiveAscendingNumbersChecker.check("232425"));
    }

    @Test
    void check_444445_true() {
        assertTrue(ConsecutiveAscendingNumbersChecker.check("444445"));
    }

    @Test
    void check_1234567_true() {
        assertTrue(ConsecutiveAscendingNumbersChecker.check("1234567"));
    }

    @Test
    void check_123412351236_true() {
        assertTrue(ConsecutiveAscendingNumbersChecker.check("123412351236"));
    }

    @Test
    void check_57585960616263_true() {
        assertTrue(ConsecutiveAscendingNumbersChecker.check("57585960616263"));
    }

    @Test
    void check_500001500002500003_true() {
        assertTrue(ConsecutiveAscendingNumbersChecker.check("500001500002500003"));
    }

    @Test
    void check_919920921_true() {
        assertTrue(ConsecutiveAscendingNumbersChecker.check("919920921"));
    }

    @Test
    void check_12341235123612371238_true() {
        assertTrue(ConsecutiveAscendingNumbersChecker.check("12341235123612371238"));
    }

    @Test
    void check_2324256_false() {
        assertFalse(ConsecutiveAscendingNumbersChecker.check("2324256"));
    }

    @Test
    void check_1235_false() {
        assertFalse(ConsecutiveAscendingNumbersChecker.check("1235"));
    }

    @Test
    void check_121316_false() {
        assertFalse(ConsecutiveAscendingNumbersChecker.check("121316"));
    }

    @Test
    void check_12131213_false() {
        assertFalse(ConsecutiveAscendingNumbersChecker.check("12131213"));
    }

    @Test
    void check_54321_false() {
        assertFalse(ConsecutiveAscendingNumbersChecker.check("54321"));
    }

    @Test
    void check_56555453_false() {
        assertFalse(ConsecutiveAscendingNumbersChecker.check("56555453"));
    }

    @Test
    void check_90090190290_false() {
        assertFalse(ConsecutiveAscendingNumbersChecker.check("90090190290"));
    }

    @Test
    void check_35236237238_false() {
        assertFalse(ConsecutiveAscendingNumbersChecker.check("35236237238"));
    }
}
