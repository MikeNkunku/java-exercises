package com.nkunku.javaexercises.edabit.expert;

import java.util.function.Consumer;

import org.junit.jupiter.api.Test;

import com.nkunku.javaexercises.edabit.expert.CoffeeShop.MenuItem;
import com.nkunku.javaexercises.edabit.expert.CoffeeShop.MenuItem.Types;

import nl.altindag.log.LogCaptor;

import static org.junit.jupiter.api.Assertions.*;

class CoffeeShopTest {

    private static final LogCaptor logCaptor = LogCaptor.forClass(CoffeeShop.class);

    @Test
    void test01() {
        MenuItem[] menu = new MenuItem[]{
                new MenuItem("orange juice", Types.DRINK, 2.13),
                new MenuItem("lemonade", Types.DRINK, 0.85),
                new MenuItem("cranberry juice", Types.DRINK, 3.36),
                new MenuItem("pineapple juice", Types.DRINK, 1.89),
                new MenuItem("lemon iced tea", Types.DRINK, 1.28),
                new MenuItem("apple iced tea", Types.DRINK, 1.28),
                new MenuItem("vanilla chai latte", Types.DRINK, 2.48),
                new MenuItem("hot chocolate", Types.DRINK, 0.99),
                new MenuItem("iced coffee", Types.DRINK, 1.12),
                new MenuItem("tuna sandwich", Types.FOOD, 0.95),
                new MenuItem("ham and cheese sandwich", Types.FOOD, 1.35),
                new MenuItem("bacon and egg", Types.FOOD, 1.15),
                new MenuItem("steak", Types.FOOD, 3.28),
                new MenuItem("hamburger", Types.FOOD, 1.05),
                new MenuItem("cinnamon roll", Types.FOOD, 1.05)};
        CoffeeShop shop = new CoffeeShop("Deep Into Coffee", menu);

        consumeAndAssertLogMessage(shop, cs -> cs.addOrder("cinnamon roll"), CoffeeShop.ORDER_ADDED_MSG);
        consumeAndAssertLogMessage(shop, cs -> cs.addOrder("iced coffee"), CoffeeShop.ORDER_ADDED_MSG);

        assertArrayEquals(new String[]{"cinnamon roll", "iced coffee"}, shop.listOrders());
        assertEquals(2.17, shop.getDueAmount(), 6);
        consumeAndAssertLogMessage(shop, CoffeeShop::fulfillOrder, "The cinnamon roll is ready!");
        consumeAndAssertLogMessage(shop, CoffeeShop::fulfillOrder, "The iced coffee is ready!");
        consumeAndAssertLogMessage(shop, CoffeeShop::fulfillOrder, CoffeeShop.ALL_ORDERS_FULFILLED_MSG);
        consumeAndAssertLogMessage(shop, cs -> cs.addOrder("hot cocoa"), CoffeeShop.UNAVAILABLE_ITEM_MSG);
        consumeAndAssertLogMessage(shop, cs -> cs.addOrder("iced tea"), CoffeeShop.UNAVAILABLE_ITEM_MSG);
        assertArrayEquals(new String[]{}, shop.listOrders());
        assertEquals(0.0, shop.getDueAmount(), 6);
        assertEquals("lemonade", shop.getCheapestItemName());
        assertArrayEquals(new String[]{"orange juice", "lemonade", "cranberry juice", "pineapple juice",
                "lemon iced tea", "apple iced tea", "vanilla chai latte", "hot chocolate", "iced coffee"},
                shop.getDrinkNamesOnly());
        assertArrayEquals(new String[]{"tuna sandwich", "ham and cheese sandwich", "bacon and egg", "steak",
                "hamburger", "cinnamon roll"}, shop.getFoodNamesOnly());
    }

    private static void consumeAndAssertLogMessage(final CoffeeShop coffeeShop,
                                                   final Consumer<CoffeeShop> coffeeShopConsumer,
                                                   final String expectedMessage) {
        coffeeShopConsumer.accept(coffeeShop);
        assertEquals(1, logCaptor.getInfoLogs().size());
        assertEquals(expectedMessage, logCaptor.getInfoLogs().get(0));
        logCaptor.clearLogs();
    }

    @Test
    void test02() {
        MenuItem[] menu = new MenuItem[]{
                new MenuItem("turkey english muffin", Types.FOOD, 7.99),
                new MenuItem("avocado toast", Types.FOOD, 5.05),
                new MenuItem("chocolate croissant", Types.FOOD, 3.00),
                new MenuItem("espresso", Types.DRINK, 2.99),
                new MenuItem("iced caramel macchiato", Types.DRINK, 4.50),
                new MenuItem("cortado", Types.DRINK, 4.00),
                new MenuItem("nitro cold brew tester", Types.DRINK, 8.00)};
        CoffeeShop shop = new CoffeeShop("Xavier's", menu);

        consumeAndAssertLogMessage(shop, cs -> cs.addOrder("cortado"), CoffeeShop.ORDER_ADDED_MSG);
        assertEquals(4.0, shop.getDueAmount(), 6);
        consumeAndAssertLogMessage(shop, CoffeeShop::fulfillOrder, "The cortado is ready!");
        consumeAndAssertLogMessage(shop, CoffeeShop::fulfillOrder, CoffeeShop.ALL_ORDERS_FULFILLED_MSG);
        consumeAndAssertLogMessage(shop, cs -> cs.addOrder("avocado toast"), CoffeeShop.ORDER_ADDED_MSG);
        assertEquals(5.05, shop.getDueAmount(), 6);
        assertArrayEquals(new String[]{"avocado toast"}, shop.listOrders());
        assertEquals("espresso", shop.getCheapestItemName());
        assertArrayEquals(new String[]{"espresso", "iced caramel macchiato", "cortado", "nitro cold brew tester"},
                shop.getDrinkNamesOnly());
        assertArrayEquals(new String[]{"turkey english muffin", "avocado toast", "chocolate croissant"},
                shop.getFoodNamesOnly());
    }

    @Test
    void test03() {
        MenuItem[] menu = new MenuItem[]{
                new MenuItem("cheeseburger with fries", Types.FOOD, 5.44),
                new MenuItem("cinnamon roll", Types.FOOD, 4.99),
                new MenuItem("hot chocolate", Types.DRINK, 2.99),
                new MenuItem("lemon tea", Types.DRINK, 2.50),
                new MenuItem("iced coffee", Types.DRINK, 3.00),
                new MenuItem("vanilla chai latte", Types.DRINK, 4.00)};
        CoffeeShop shop = new CoffeeShop("Tesha's", menu);

        consumeAndAssertLogMessage(shop, cs -> cs.addOrder("hot cocoa"), CoffeeShop.UNAVAILABLE_ITEM_MSG);
        assertEquals(0.0, shop.getDueAmount(), 6);
        consumeAndAssertLogMessage(shop, cs -> cs.addOrder("cheeseburger with fries"), CoffeeShop.ORDER_ADDED_MSG);
        consumeAndAssertLogMessage(shop, cs -> cs.addOrder("lemon tea"), CoffeeShop.ORDER_ADDED_MSG);
        consumeAndAssertLogMessage(shop, cs -> cs.addOrder("iced coffee"), CoffeeShop.ORDER_ADDED_MSG);
        assertArrayEquals(new String[]{"cheeseburger with fries", "lemon tea", "iced coffee"}, shop.listOrders());
        assertEquals(10.94, shop.getDueAmount(), 6);
        consumeAndAssertLogMessage(shop, CoffeeShop::fulfillOrder, "The cheeseburger with fries is ready!");
        consumeAndAssertLogMessage(shop, CoffeeShop::fulfillOrder, "The lemon tea is ready!");
        consumeAndAssertLogMessage(shop, CoffeeShop::fulfillOrder, "The iced coffee is ready!");
        assertArrayEquals(new String[]{}, shop.listOrders());
        assertEquals("lemon tea", shop.getCheapestItemName());
        assertArrayEquals(new String[]{"hot chocolate", "lemon tea", "iced coffee", "vanilla chai latte"},
                shop.getDrinkNamesOnly());
        assertArrayEquals(new String[]{"cheeseburger with fries", "cinnamon roll"}, shop.getFoodNamesOnly());
    }
}
