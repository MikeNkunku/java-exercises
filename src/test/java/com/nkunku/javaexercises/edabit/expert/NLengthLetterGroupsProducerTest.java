package com.nkunku.javaexercises.edabit.expert;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NLengthLetterGroupsProducerTest {

    @Test
    void produce_useCase01() {
        assertArrayEquals(new String[] {"ationa", "interc", "ntalis", "ontine"},
                NLengthLetterGroupsProducer.produce("intercontinentalisationalism", 6));
    }

    @Test
    void produce_useCase02() {
        assertArrayEquals(new String[] {"eng", "str", "ths"}, NLengthLetterGroupsProducer.produce("strengths", 3));
    }

    @Test
    void produce_useCase03() {
        assertArrayEquals(new String[] {"croscopicsilico", "pneumonoultrami", "volcanoconiosis"},
                NLengthLetterGroupsProducer.produce("pneumonoultramicroscopicsilicovolcanoconiosis", 15));
    }

    @Test
    void produce_useCase04() {
        assertArrayEquals(new String[] {"aphi", "call", "cogr", "lexi"},
                NLengthLetterGroupsProducer.produce("lexicographically", 4));
    }

    @Test
    void produce_useCase05() {
        assertArrayEquals(new String[] {"anesth", "esiolo"},
                NLengthLetterGroupsProducer.produce("anesthesiologists", 6));
    }

    @Test
    void produce_useCase06() {
        assertArrayEquals(new String[] {"matogl", "subder"}, NLengthLetterGroupsProducer.produce("subdermatoglyphic", 6));
    }

    @Test
    void produce_useCase07() {
        assertArrayEquals(new String[] {"pedali", "sesqui"}, NLengthLetterGroupsProducer.produce("sesquipedalianism", 6));
    }

    @Test
    void produce_useCase08() {
        assertArrayEquals(new String[] {"ect", "ion", "oll", "rec"},
                NLengthLetterGroupsProducer.produce("recollection", 3));
    }

    @Test
    void produce_useCase09() {
        assertArrayEquals(new String[] {"hyroidi", "poparat", "pseudop", "seudohy"},
                NLengthLetterGroupsProducer.produce("pseudopseudohypoparathyroidism", 7));
    }

    @Test
    void produce_useCase10() {
        assertArrayEquals(new String[] {"at", "ci", "fl", "ic", "if", "ih", "il", "il", "in", "io", "ip", "na", "oc",
                "uc"}, NLengthLetterGroupsProducer.produce("floccinaucinihilipilification", 2));
    }

    @Test
    void produce_useCase11() {
        assertArrayEquals(new String[] {"ablis", "antid", "arian", "hment", "isest"},
                NLengthLetterGroupsProducer.produce("antidisestablishmentarianism", 5));
    }

    @Test
    void produce_useCase12() {
        assertArrayEquals(new String[] {"ali", "ali", "doc", "erc", "fra", "gil", "ice", "iou", "ist", "sup", "xpi"},
                NLengthLetterGroupsProducer.produce("supercalifragilisticexpialidocious", 3));
    }

    @Test
    void produce_useCase13() {
        assertArrayEquals(new String[] {"ensibilit", "incompreh"},
                NLengthLetterGroupsProducer.produce("incomprehensibilities", 9));
    }

    @Test
    void produce_useCase14() {
        assertArrayEquals(new String[] {"astr", "ophy", "sici"},
                NLengthLetterGroupsProducer.produce("astrophysicists", 4));
    }

    @Test
    void produce_useCase15() {
        assertArrayEquals(new String[] {"honorificabi", "litudinitati"},
                NLengthLetterGroupsProducer.produce("honorificabilitudinitatibus", 12));
    }

    @Test
    void produce_useCase16() {
        assertArrayEquals(new String[] {"unimagin"}, NLengthLetterGroupsProducer.produce("unimaginatively", 8));
    }

    @Test
    void produce_useCase17() {
        assertArrayEquals(new String[] {}, NLengthLetterGroupsProducer.produce("euouae", 7));
    }

    @Test
    void produce_useCase18() {
        assertArrayEquals(new String[] {"tsktsk"}, NLengthLetterGroupsProducer.produce("tsktsk", 6));
    }

    @Test
    void produce_useCase19() {
        assertArrayEquals(new String[] {"uncopyright"}, NLengthLetterGroupsProducer.produce("uncopyrightable", 11));
    }

    @Test
    void produce_useCase20() {
        assertArrayEquals(new String[] {"har", "ing", "pen", "tes"},
                NLengthLetterGroupsProducer.produce("tesharpening", 3));
    }


    @Test
    void produce_useCase21() {
        assertArrayEquals(new String[] {"holic", "tesha"}, NLengthLetterGroupsProducer.produce("teshaholic", 5));
    }
}
