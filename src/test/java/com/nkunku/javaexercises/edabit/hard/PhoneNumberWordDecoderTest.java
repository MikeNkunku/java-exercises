package com.nkunku.javaexercises.edabit.hard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PhoneNumberWordDecoderTest {

    @Test
    void decode_useCase1() {
        assertEquals("123-647-3937", PhoneNumberWordDecoder.decode("123-647-EYES"));
    }

    @Test
    void decode_useCase2() {
        assertEquals("(325)444-8378", PhoneNumberWordDecoder.decode("(325)444-TEST"));
    }

    @Test
    void decode_useCase3() {
        assertEquals("653-879-8447", PhoneNumberWordDecoder.decode("653-TRY-THIS"));
    }

    @Test
    void decode_useCase4() {
        assertEquals("435-224-7613", PhoneNumberWordDecoder.decode("435-224-7613"));
    }

    @Test
    void decode_useCase5() {
        assertEquals("(333)668-3245", PhoneNumberWordDecoder.decode("(33D)ONT-FAIL"));
    }

    @Test
    void decode_useCase6() {
        assertEquals("(025)445-6741", PhoneNumberWordDecoder.decode("(025)445-6741"));
    }
}
