package com.nkunku.javaexercises.edabit.hard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FactorialCheckerTest {

    @Test
    void check_useCase1_true(){
        assertTrue(FactorialChecker.check(2));
    }

    @Test
    void check_useCase2_false(){
        assertFalse(FactorialChecker.check(16));
    }

    @Test
    void check_useCase3_true(){
        assertTrue(FactorialChecker.check(24));
    }

    @Test
    void check_useCase4_false(){
        assertFalse(FactorialChecker.check(36));
    }

    @Test
    void check_useCase5_true(){
        assertTrue(FactorialChecker.check(120));
    }

    @Test
    void check_useCase6_false(){
        assertFalse(FactorialChecker.check(721));
    }
}
