package com.nkunku.javaexercises.edabit.hard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfPrimesGetterTest {

    @Test
    void get_useCase1() {
        assertEquals(8, NumberOfPrimesGetter.get(20));
    }

    @Test
    void get_useCase2() {
        assertEquals(10, NumberOfPrimesGetter.get(30));
    }

    @Test
    void get_useCase3() {
        assertEquals(25, NumberOfPrimesGetter.get(100));
    }

    @Test
    void get_useCase4() {
        assertEquals(46, NumberOfPrimesGetter.get(200));
    }

    @Test
    void get_useCase5() {
        assertEquals(168, NumberOfPrimesGetter.get(1000));
    }

    @Test
    void get_useCase6() {
        assertEquals(0, NumberOfPrimesGetter.get(-5));
    }

    @Test
    void get_useCase7() {
        assertEquals(18, NumberOfPrimesGetter.get(66));
    }

    @Test
    void get_useCase8() {
        assertEquals(32, NumberOfPrimesGetter.get(133));
    }

    @Test
    void get_useCase9() {
        assertEquals(25, NumberOfPrimesGetter.get(99));
    }
}
