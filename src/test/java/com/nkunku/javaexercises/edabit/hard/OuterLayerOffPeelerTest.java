package com.nkunku.javaexercises.edabit.hard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OuterLayerOffPeelerTest {

    @Test
    void peelOff_useCase1() {
        assertArrayEquals(
            new Character[][]{
                {'f', 'g'},
                {'j', 'k'}
            },
            OuterLayerOffPeeler.peelOff(
                new Character[][]{
                    {'a', 'b', 'c', 'd'},
                    {'e', 'f', 'g', 'h'},
                    {'i', 'j', 'k', 'l'},
                    {'m', 'n', 'o', 'p'}
                }
            )
        );
    }

    @Test
    void peelOff_useCase2() {
        assertArrayEquals(
            new Integer[][]{
                {7,  8,  9},
                {12, 13, 14},
                {17, 18, 19},
                {22, 23, 24},
                {27, 28, 29}
            },
            OuterLayerOffPeeler.peelOff(
                new Integer[][]{
                    {1,  2,  3,  4, 5},
                    {6,  7,  8,  9, 10},
                    {11, 12, 13, 14, 15},
                    {16, 17, 18, 19, 20},
                    {21, 22, 23, 24, 25},
                    {26, 27, 28, 29, 30},
                    {31, 32, 33, 34, 35}
                }
            )
        );
    }

    @Test
    void peelOff_useCase3() {
        assertArrayEquals(
            new Boolean[][]{{false}},
            OuterLayerOffPeeler.peelOff(
                    new Boolean[][]{
                            {true, false, true},
                            {false, false, true},
                            {true, true, true}
                    }
            )
        );
    }

    @Test
    void peelOff_useCase4() {
        assertArrayEquals(
            new String[0],
            OuterLayerOffPeeler.peelOff(
                new String[][]{
                    {"hello", "world"},
                    {"hello", "world"}
                }
            )
        );
    }

    @Test
    void peelOff_useCase5() {
        assertArrayEquals(
            new Object[][]{{false, true, 5, 6, 7}},
            OuterLayerOffPeeler.peelOff(
                new Object[][]{
                    {true, false, true, 1, 2, 3, 4},
                    {false, false, true, 5, 6, 7, 8},
                    {true, true, true, 9, 10, 11, 12}
                }
            )
        );
    }
}
