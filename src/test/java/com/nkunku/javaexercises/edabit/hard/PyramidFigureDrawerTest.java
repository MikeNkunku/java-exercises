package com.nkunku.javaexercises.edabit.hard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PyramidFigureDrawerTest {

    @Test
    void draw_with1Line_exceptionThrown() {
        try {
            PyramidFigureDrawer.draw(1);
            fail("An exception should have been thrown by then");
        } catch (final Exception exception) {
            assertTrue(exception instanceof IllegalArgumentException);
            assertEquals("Only positive integers equal or greater than 2 are supported",
                    exception.getMessage());
        }
    }

    @Test
    void draw_with2Lines() {
        String res =
            "////\\\\\\\\\n" +
            "********\n";
        assertEquals(res, PyramidFigureDrawer.draw(2));
    }

    @Test
    void draw_with3Lines() {
        String res =
            "////////\\\\\\\\\\\\\\\\\n" +
            "////********\\\\\\\\\n" +
            "****************\n";
        assertEquals(res, PyramidFigureDrawer.draw(3));
    }

    @Test
    void draw_with4Lines() {
        String res =
            "////////////\\\\\\\\\\\\\\\\\\\\\\\\\n" +
            "////////********\\\\\\\\\\\\\\\\\n" +
            "////****************\\\\\\\\\n" +
            "************************\n";
        assertEquals(res, PyramidFigureDrawer.draw(4));
    }

    @Test
    void draw_with5Lines() {
        String res =
            "////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\n" +
            "////////////********\\\\\\\\\\\\\\\\\\\\\\\\\n" +
            "////////****************\\\\\\\\\\\\\\\\\n" +
            "////************************\\\\\\\\\n" +
            "********************************\n";
        assertEquals(res, PyramidFigureDrawer.draw(5));
    }

    @Test
    void draw_with6Lines() {
        String res =
            "////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\n" +
            "////////////////********\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\n" +
            "////////////****************\\\\\\\\\\\\\\\\\\\\\\\\\n" +
            "////////************************\\\\\\\\\\\\\\\\\n" +
            "////********************************\\\\\\\\\n" +
            "****************************************\n";
        assertEquals(res, PyramidFigureDrawer.draw(6));
    }
}
