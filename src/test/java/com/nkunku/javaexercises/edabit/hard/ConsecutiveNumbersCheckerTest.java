package com.nkunku.javaexercises.edabit.hard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConsecutiveNumbersCheckerTest {

    @Test
    void check_useCase1_true() {
        assertTrue(ConsecutiveNumbersChecker.check(new int[]{5, 1, 4, 3, 2}));
    }

    @Test
    void check_useCase2_true() {
        assertTrue(ConsecutiveNumbersChecker.check(new int[]{55, 59, 58, 56, 57}));
    }

    @Test
    void check_useCase3_true() {
        assertTrue(ConsecutiveNumbersChecker.check(new int[]{-3, -2, -1, 1, 0}));
    }

    @Test
    void check_useCase4_false() {
        assertFalse(ConsecutiveNumbersChecker.check(new int[]{5, 1, 4, 3, 2, 8}));
    }

    @Test
    void check_useCase5_false() {
        assertFalse(ConsecutiveNumbersChecker.check(new int[]{5, 6, 7, 8, 9, 9}));
    }

    @Test
    void check_useCase6_false() {
        assertFalse(ConsecutiveNumbersChecker.check(new int[]{5, 3}));
    }
}
