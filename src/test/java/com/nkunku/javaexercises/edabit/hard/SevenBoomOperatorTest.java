package com.nkunku.javaexercises.edabit.hard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SevenBoomOperatorTest {

    @Test
    void operate_useCase1_ok() {
        assertEquals(SevenBoomOperator.SEVEN_FOUND_MESSAGE, SevenBoomOperator.operate(new int[]{2, 6, 7, 9, 3}));
    }

    @Test
    void operate_useCase2_ko() {
        assertEquals(SevenBoomOperator.SEVEN_NOT_FOUND_MESSAGE, SevenBoomOperator.operate(new int[]{33, 68, 400, 5}));
    }

    @Test
    void operate_useCase3_ko() {
        assertEquals(SevenBoomOperator.SEVEN_NOT_FOUND_MESSAGE, SevenBoomOperator.operate(new int[]{86, 48, 100, 66}));
    }

    @Test
    void operate_useCase4_ok() {
        assertEquals(SevenBoomOperator.SEVEN_FOUND_MESSAGE, SevenBoomOperator.operate(new int[]{76, 55, 44, 32}));
    }

    @Test
    void operate_useCase5_ok() {
        assertEquals(SevenBoomOperator.SEVEN_FOUND_MESSAGE, SevenBoomOperator.operate(new int[]{35, 4, 9, 37}));
    }
}
