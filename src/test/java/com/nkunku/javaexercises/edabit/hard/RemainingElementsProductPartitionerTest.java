package com.nkunku.javaexercises.edabit.hard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RemainingElementsProductPartitionerTest {

    @Test
    void canPartition_useCase01_false() {
        assertFalse(RemainingElementsProductPartitioner.canPartition(new int[]{-1, -10, 1, -2, 20}));
    }

    @Test
    void canPartition_useCase02_true() {
        assertTrue(RemainingElementsProductPartitioner.canPartition(new int[]{-1, -20, 5, -1, -2, 2}));
    }

    @Test
    void canPartition_useCase03_true() {
        assertTrue(RemainingElementsProductPartitioner.canPartition(new int[]{2, 8, 4, 1}));
    }

    @Test
    void canPartition_useCase04_false() {
        assertFalse(RemainingElementsProductPartitioner.canPartition(new int[]{1, 1, -1, 1}));
    }

    @Test
    void canPartition_useCase05_true() {
        assertTrue(RemainingElementsProductPartitioner.canPartition(new int[]{-1, -1, 1, 1}));
    }

    @Test
    void canPartition_useCase06_false() {
        assertFalse(RemainingElementsProductPartitioner.canPartition(new int[]{0, 5, 1, -1}));
    }

    @Test
    void canPartition_useCase07_false() {
        assertFalse(RemainingElementsProductPartitioner.canPartition(new int[]{0, 1, 1, -1}));
    }

    @Test
    void canPartition_useCase08_false() {
        assertFalse(RemainingElementsProductPartitioner.canPartition(new int[]{0, 1, 1, 1}));
    }

    @Test
    void canPartition_useCase09_true() {
        assertTrue(RemainingElementsProductPartitioner.canPartition(new int[]{0, 0, 1, 1}));
    }

    @Test
    void canPartition_useCase10_true() {
        assertTrue(RemainingElementsProductPartitioner.canPartition(new int[]{0, 0, 1}));
    }

    @Test
    void canPartition_useCase11_true() {
        assertTrue(RemainingElementsProductPartitioner.canPartition(new int[]{0, 0, 0}));
    }

    @Test
    void canPartition_useCase12_false() {
        assertFalse(RemainingElementsProductPartitioner.canPartition(new int[]{5, 5, 25, 100}));
    }

    @Test
    void canPartition_useCase13_false() {
        assertFalse(RemainingElementsProductPartitioner.canPartition(new int[]{-1, 5, 20, 100}));
    }

    @Test
    void canPartition_useCase14_true() {
        assertTrue(RemainingElementsProductPartitioner.canPartition(new int[]{1, 1, 1, 1}));
    }

    @Test
    void canPartition_useCase15_true() {
        assertTrue(RemainingElementsProductPartitioner.canPartition(new int[]{-1, 1, -1}));
    }

    @Test
    void canPartition_useCase16_false() {
        assertFalse(RemainingElementsProductPartitioner.canPartition(new int[]{-1, 1, 1}));
    }
}
