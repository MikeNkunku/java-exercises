package com.nkunku.javaexercises.edabit.hard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AlmostPalindromeCheckerTest {

    @Test
    void check_useCase01_true() {
        assertTrue(AlmostPalindromeChecker.check("abcdcbg"));
    }

    @Test
    void check_useCase02_true() {
        assertTrue(AlmostPalindromeChecker.check("abccia"));
    }

    @Test
    void check_useCase03_false() {
        assertFalse(AlmostPalindromeChecker.check("abcdaaa"));
    }

    @Test
    void check_useCase04_true() {
        assertTrue(AlmostPalindromeChecker.check("gggfgig"));
    }

    @Test
    void check_useCase05_false() {
        assertFalse(AlmostPalindromeChecker.check("gggffff"));
    }

    @Test
    void check_useCase06_true() {
        assertTrue(AlmostPalindromeChecker.check("GIGGG"));
    }

    @Test
    void check_useCase07_true() {
        assertTrue(AlmostPalindromeChecker.check("ggggi"));
    }

    @Test
    void check_useCase08_false() {
        assertFalse(AlmostPalindromeChecker.check("ggggg"));
    }

    @Test
    void check_useCase09_false() {
        assertFalse(AlmostPalindromeChecker.check("gggfggg"));
    }

    @Test
    void check_useCase10_false() {
        assertFalse(AlmostPalindromeChecker.check("1234312"));
    }
}
