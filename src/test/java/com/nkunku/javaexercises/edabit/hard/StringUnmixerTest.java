package com.nkunku.javaexercises.edabit.hard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringUnmixerTest {

    @Test
    void unmix_useCase1() {
        assertEquals("214365", StringUnmixer.unmix("123456"));
    }

    @Test
    void unmix_useCase2() {
        assertEquals("This is a mixed up string.", StringUnmixer.unmix("hTsii  s aimex dpus rtni.g"));
    }

    @Test
    void unmix_useCase3() {
        assertEquals("abcde", StringUnmixer.unmix("badce"));
    }

    @Test
    void unmix_useCase4() {
        assertEquals("I am feeling a little dizzy!", StringUnmixer.unmix(" Imaf eeilgna l tilt eidzz!y"));
    }

    @Test
    void unmix_useCase5() {
        assertEquals("", StringUnmixer.unmix(""));
    }
}
