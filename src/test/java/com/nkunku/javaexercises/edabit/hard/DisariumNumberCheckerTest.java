package com.nkunku.javaexercises.edabit.hard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DisariumNumberCheckerTest {

    @Test
    void check_useCase01_true() {
        assertTrue(DisariumNumberChecker.check(6));
    }

    @Test
    void check_useCase02_false() {
        assertFalse(DisariumNumberChecker.check(75));
    }

    @Test
    void check_useCase03_true() {
        assertTrue(DisariumNumberChecker.check(135));
    }

    @Test
    void check_useCase04_false() {
        assertFalse(DisariumNumberChecker.check(466));
    }

    @Test
    void check_useCase05_false() {
        assertFalse(DisariumNumberChecker.check(372));
    }

    @Test
    void check_useCase06_true() {
        assertTrue(DisariumNumberChecker.check(175));
    }

    @Test
    void check_useCase07_true() {
        assertTrue(DisariumNumberChecker.check(1));
    }

    @Test
    void check_useCase08_false() {
        assertFalse(DisariumNumberChecker.check(696));
    }

    @Test
    void check_useCase09_false() {
        assertFalse(DisariumNumberChecker.check(876));
    }

    @Test
    void check_useCase10_true() {
        assertTrue(DisariumNumberChecker.check(89));
    }

    @Test
    void check_useCase11_true() {
        assertTrue(DisariumNumberChecker.check(518));
    }

    @Test
    void check_useCase12_true() {
        assertTrue(DisariumNumberChecker.check(598));
    }

    @Test
    void check_useCase13_false() {
        assertFalse(DisariumNumberChecker.check(544));
    }

    @Test
    void check_useCase14_false() {
        assertFalse(DisariumNumberChecker.check(466));
    }
}
