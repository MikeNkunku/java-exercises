package com.nkunku.javaexercises.edabit.hard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class XPhoneticConverterTest {

    @Test
    void convert_useCase1() {
        assertEquals("Inside the bocks was a zylophone",
                XPhoneticConverter.convert("Inside the box was a xylophone"));
    }

    @Test
    void convert_useCase2() {
        assertEquals("The ecks ray is eckscellent",
                XPhoneticConverter.convert("The x ray is excellent"));
    }

    @Test
    void convert_useCase3() {
        assertEquals("OMG ecks bocks unbocksing video ecks D",
                XPhoneticConverter.convert("OMG x box unboxing video x D"));
    }

    @Test
    void convert_useCase4() {
        assertEquals("I gotta make bucks but the clocks are ticking!",
                XPhoneticConverter.convert("I gotta make bux but the clox are ticking!"));
    }

    @Test
    void convert_useCase5() {
        assertEquals("this test does not have an ecks in it",
                XPhoneticConverter.convert("this test does not have an x in it"));
    }

    @Test
    void convert_useCase6() {
        assertEquals("Macks backs packs", XPhoneticConverter.convert("Max bax pax"));
    }

    @Test
    void convert_useCase7() {
        assertEquals("Anti vacks", XPhoneticConverter.convert("Anti vax"));
    }

    @Test
    void convert_useCase8() {
        assertEquals("Who is zavier and why does he have my car",
                XPhoneticConverter.convert("Who is xavier and why does he have my car"));
    }

    @Test
    void convert_useCase9() {
        assertEquals("OMG zylem unbocksing video ecks D",
                XPhoneticConverter.convert("OMG xylem unboxing video x D"));
    }
}
