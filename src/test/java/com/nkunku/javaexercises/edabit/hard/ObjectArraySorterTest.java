package com.nkunku.javaexercises.edabit.hard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ObjectArraySorterTest {

    @Test
    void getSortedArray_useCase1() {
        assertArrayEquals(new Object[] {1, 3, 4}, ObjectArraySorter.getSortedArray(new Object[] {4, 1, 3}));
    }

    @Test
    void getSortedArray_useCase2() {
        assertArrayEquals(new Object[] {new int[] {1}, new int[] {3}, new int[] {4}},
                ObjectArraySorter.getSortedArray(new Object[] {new int[] {4}, new int[] {1}, new int[] {3}}));
    }

    @Test
    void getSortedArray_useCase3() {
        assertArrayEquals(new Object[] {12, new int[] {12}, 13, 17, 1979, new int[] {2000}},
                ObjectArraySorter.getSortedArray(new Object[] {13, new int[] {2000},1979, 12, new int[] {12}, 17}));
    }

    @Test
    void getSortedArray_useCase4() {
        assertArrayEquals(new Object[] {new int[] {1}, 3, 4},
                ObjectArraySorter.getSortedArray(new Object[] {4, new int[] {1}, 3}));
    }

    @Test
    void getSortedArray_useCase5() {
        assertArrayEquals(new Object[] {1, new int[] {3}, new int[] {4}},
                ObjectArraySorter.getSortedArray(new Object[] {new int[] {4}, 1, new int[] {3}}));
    }

    @Test
    void getSortedArray_useCase6() {
        assertArrayEquals(new Object[] {1, new int[] {2}, new int[] {3}, 4, new int[] {5}, 6},
                ObjectArraySorter.getSortedArray(new Object[] {new int[] {3}, 4, new int[] {2}, new int[] {5}, 1, 6}));
    }

    @Test
    void getSortedArray_useCase7() {
        assertArrayEquals(new Object[] {1, new int[] {3}, new int[] {5}, 6, 7, new int[] {9}},
                ObjectArraySorter.getSortedArray(new Object[] {new int[] {3}, 7, new int[] {9}, new int[] {5}, 1, 6}));
    }

    @Test
    void getSortedArray_useCase8() {
        assertArrayEquals(new Object[] {1, new int[] {3}, new int[] {5}, 6, 7, new int[] {8}, new int[] {9}},
                ObjectArraySorter.getSortedArray(new Object[] {new int[] {3}, 7, new int[] {9}, new int[] {5},1, 6,
                        new int[] {8}}));
    }
}
