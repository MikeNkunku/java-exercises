package com.nkunku.javaexercises.codeexercises;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CaesarCipherDecoderTest {

	@Test
	void decode_diozmqdzr_interview() {
		assertEquals("interview", CaesarCipherDecoder.decode("diozmqdzr"));
	}

	@Test
	void decode_evqv_java() {
		assertEquals("java", CaesarCipherDecoder.decode("evqv"));
	}

	@Test
	void decode_xvm_car() {
		assertEquals("car", CaesarCipherDecoder.decode("xvm"));
	}
}
