package com.nkunku.javaexercises.codeexercises;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LongestPalindromeFinderTest {

	@Test
	void find_abattd_aba() {
		assertEquals("aba", LongestPalindromeFinder.find("abattd"));
	}

	@Test
	void find_abc_a() {
		assertEquals("a", LongestPalindromeFinder.find("abc"));
	}

	@Test
	void find_abcba_abcba() {
		assertEquals("abcba", LongestPalindromeFinder.find("abcba"));
	}

	@Test
	void find_thhwerw_hh() {
		assertEquals("hh", LongestPalindromeFinder.find("thhwerw"));
	}
}
