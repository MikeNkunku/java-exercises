package com.nkunku.javaexercises.codeacademy.beginner;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PangramCheckerTest {

    @Test
    void check_withAllLetters_useCase1() {
        assertTrue(PangramChecker.check("The quick brown fox jumps over the lazy dog."));
    }

    @Test
    void check_withAllLetters_useCase2() {
        assertTrue(PangramChecker.check("Mr. Jock, TV quiz Ph.D., bags few lynx."));
    }

    @Test
    void check_withLettersMissing() {
        assertFalse(PangramChecker.check("This is a test."));
    }
}
