package com.nkunku.javaexercises.codeacademy.advanced;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrimeNumbersFinderTest {

    @Test
    void check() {
        final int[] numbers = {1, 3, 5, 7, 9, 11, 15};
        final int[] expectedPrimeNumbers = {1, 3, 5, 7, 11};

        assertArrayEquals(expectedPrimeNumbers, PrimeNumbersFinder.find(numbers));
    }
}
