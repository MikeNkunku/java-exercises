package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import java.util.Map;

import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.Test;

import com.nkunku.javaexercises.w3resource.basicexercisespartone.CirclePerimeterAndAreaCalculator.Keys;

import static org.junit.jupiter.api.Assertions.*;

class CirclePerimeterAndAreaCalculatorTest {

	@Test
	void calculate() {
		final Map<String, Double> map = CirclePerimeterAndAreaCalculator.calculate(7.5d);

		assertEquals(2, map.size());
		assertEquals(47.12d, Precision.round(map.get(Keys.PERIMETER.id()).doubleValue(), 2));
		assertEquals(176.71d, Precision.round(map.get(Keys.AREA.id()).doubleValue(), 2));
	}
}
