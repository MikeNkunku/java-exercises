package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import java.util.Map;

import org.junit.jupiter.api.Test;

import com.nkunku.javaexercises.w3resource.basicexercisespartone.CharactersPerCategoryCounter.Categories;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CharactersPerCategoryCounterTest {

	@Test
	void count() {
		final String text = "Aa kiu, I swd skieo 236587. GH kiu: sieo?? 25.33";

		final Map<Categories, Long> map = CharactersPerCategoryCounter.count(text);

		assertEquals(Long.valueOf(23L), map.get(Categories.LETTERS));
		assertEquals(Long.valueOf(9L), map.get(Categories.SPACES));
		assertEquals(Long.valueOf(10L), map.get(Categories.NUMBERS));
		assertEquals(Long.valueOf(6L), map.get(Categories.MISCELLANEOUS));
	}
}
