package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FirstNPrimeNumbersSumOperatorTest {

	@Test
	void sum_first100PrimeNumbers_21086() {
		assertEquals(21_086L, FirstNPrimeNumbersSumOperator.sum(100L));
	}

	@Test
	void sum_first2PrimeNumbers_5() {
		assertEquals(5L, FirstNPrimeNumbersSumOperator.sum(2L));
	}
}
