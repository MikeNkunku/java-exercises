package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DecimalNumberToBinaryNumberConverterTest {

	@Test
	void convert() {
		assertEquals(101, DecimalNumberToBinaryNumberConverter.convert(5));
	}
}
