package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collections;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

class FileSizeGetterTest {

	@Test
	void get() throws Exception {
		final File file = File.createTempFile("test-file",".txt");
		file.deleteOnExit();

		Files.write(file.toPath(), Collections.singleton("some text"), StandardCharsets.UTF_8);

		final long size = FileSizeGetter.get(file.getAbsolutePath());

		assertNotEquals(0L, size);
	}
}
