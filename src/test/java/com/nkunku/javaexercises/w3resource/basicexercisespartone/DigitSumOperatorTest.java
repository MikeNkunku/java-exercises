package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class DigitSumOperatorTest {

	@Test
	void sum_145_10() {
		assertEquals(10L, DigitSumOperator.sum(145L));
	}

	@Test
	void sum_withNegativeNumber_exceptionThrown() {
		try {
			DigitSumOperator.sum(-110L);
			fail("An exception should have been thrown by then");
		} catch (final Exception exception) {
			assertTrue(exception instanceof IllegalArgumentException);
			assertEquals("A positive number must be provided", exception.getMessage());
		}
	}
}
