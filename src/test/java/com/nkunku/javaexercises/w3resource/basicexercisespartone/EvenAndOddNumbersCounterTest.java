package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import java.util.Map;

import org.junit.jupiter.api.Test;

import com.nkunku.javaexercises.w3resource.basicexercisespartone.EvenAndOddNumbersCounter.Categories;

import static org.junit.jupiter.api.Assertions.*;

class EvenAndOddNumbersCounterTest {

	@Test
	void count() {
		final int[] numbers = new int[] {0, 1, 2, 3, 7, 8, 9, 12, 14, 18, 17};

		final Map<Categories, Long> categoriesMap = EvenAndOddNumbersCounter.count(numbers);

		assertEquals(5L, categoriesMap.get(Categories.ODD).longValue());
		assertEquals(6L, categoriesMap.get(Categories.EVEN).longValue());
	}
}
