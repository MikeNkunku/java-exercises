package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SentenceWordFirstLetterCapitalizerTest {

	@Test
	void capitalize_withEmptyString() {
		assertEquals("", SentenceWordFirstLetterCapitalizer.capitalize(""));
	}

	@Test
	void capitalize_withSentence() {
		final String sentence = "the quick brown fox jumps over the lazy dog.";
		final String expectation = "The Quick Brown Fox Jumps Over The Lazy Dog.";

		assertEquals(expectation, SentenceWordFirstLetterCapitalizer.capitalize(sentence));
	}
}
