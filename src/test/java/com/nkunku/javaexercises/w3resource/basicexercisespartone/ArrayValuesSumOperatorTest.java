package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayValuesSumOperatorTest {

	@Test
	void sum() {
		final int[] numbers = new int[] {45, 12, 76, 3};
		assertEquals(136L, ArrayValuesSumOperator.sum(numbers));
	}
}
