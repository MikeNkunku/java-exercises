package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SameRightmostDigitCheckerTest {

	@Test
	void check_false() {
		assertFalse(SameRightmostDigitChecker.check(5, 10, 21));
	}

	@Test
	void check_true() {
		assertTrue(SameRightmostDigitChecker.check(5, 10, 15));
	}

	@Test
	void check_withANegativeNumber_exceptionThrown() {
		try {
			SameRightmostDigitChecker.check(-1, 0, 3);
			fail("An exception should have been thrown by then");
		} catch (final Exception exception) {
			assertTrue(exception instanceof IllegalArgumentException);
			assertEquals("Only positive numbers are allowed", exception.getMessage());
		}
	}
}
