package com.nkunku.javaexercises.w3resource.basicexercisespartone;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AfterLast10ValueArrayCreatorTest {

	@Test
	void create_with10InArray() {
		final int[] numbers = new int[] {1, 10, 3, 5, 7};
		final int[] expectedArr = new int[] {3, 5, 7};

		assertArrayEquals(expectedArr, AfterLast10ValueArrayCreator.create(numbers));
	}

	@Test
	void create_without10InArray() {
		final int[] numbers = new int[] {1, 3, 5, 7};
		assertEquals(0, AfterLast10ValueArrayCreator.create(numbers).length);
	}
}
