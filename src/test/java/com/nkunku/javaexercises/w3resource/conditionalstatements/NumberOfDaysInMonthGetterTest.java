package com.nkunku.javaexercises.w3resource.conditionalstatements;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfDaysInMonthGetterTest {

    @Test
    void get() {
        assertEquals(29, NumberOfDaysInMonthGetter.get(2016, 2));
    }
}
