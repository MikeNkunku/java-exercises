package com.nkunku.javaexercises.w3resource.arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubArrayWith0SumCheckerTest {

	@Test
	void check_useCase1_true() {
		final int[] numbers = new int[] {1, 2, -2, 3, 4, 5, 6};
		assertTrue(SubArrayWith0SumChecker.check(numbers));
	}

	@Test
	void check_useCase2_false() {
		final int[] numbers = new int[] {1, 2, 3, 4, 5, 6};
		assertFalse(SubArrayWith0SumChecker.check(numbers));
	}

	@Test
	void check_useCase3_true() {
		final int[] numbers = new int[] {1, 2, -3, 4, 5, 6};
		assertTrue(SubArrayWith0SumChecker.check(numbers));
	}
}
