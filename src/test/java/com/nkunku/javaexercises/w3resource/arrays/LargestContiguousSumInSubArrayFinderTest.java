package com.nkunku.javaexercises.w3resource.arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LargestContiguousSumInSubArrayFinderTest {

    @Test
    void find() {
        final int[] numbers = {1, 2, -3, -4, 0, 6, 7, 8, 9};
        final int[] expectedSubArray = {6, 7, 8, 9};

        final int[] subArray = LargestContiguousSumInSubArrayFinder.find(numbers);

        assertArrayEquals(expectedSubArray, subArray);
    }
}
