package com.nkunku.javaexercises.w3resource.arrays;

import java.util.*;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubArrayWith0SumFinderTest {

	@Test
	void find() {
		final int[] numbers = {1, 3, -7, 3, 2, 3, 1, -3, -2, -2};
		final List<int[]> expectedSubArrays = Arrays.asList(new int[]{3, -3}, new int[]{2, -2}, new int[]{1, 2, -3},
				new int[]{1, 1, -2}, new int[]{2, 1, -3}, new int[]{1, 3, -7, 3}, new int[]{1, -7, 3, 3},
				new int[]{3, -7, 3, 1}, new int[]{3, 2, -3, -2}, new int[]{-7, 3, 3, 1}, new int[]{2, 3, -3, -2},
				new int[]{1, 3, -7, 2, 1}, new int[]{1, 3, 1, -3, -2}, new int[]{1, -7, 3, 2, 1},
				new int[]{1, -7, 2, 3, 1}, new int[]{3, -7, 3, 3, -2}, new int[]{1, 3, -7, 3, 2, -2},
				new int[]{1, 3, -7, 3, 3, -3}, new int[]{1, 3, -7, 2, 3, -2}, new int[]{1, -7, 3, 2, 3, -2},
				new int[]{3, -7, 3, 2, 1, -2}, new int[]{3, -7, 3, 3, 1, -3}, new int[]{3, -7, 2, 3, 1, -2},
				new int[]{-7, 3, 2, 3, 1, -2}, new int[]{1, 3, -7, 3, 2, 1, -3}, new int[]{1, 3, -7, 2, 3, 1, -3},
				new int[]{1, -7, 3, 2, 3, 1, -3}, new int[]{1, 3, -7, 3, 2, 3, -3, -2},
				new int[]{3, -7, 3, 2, 3, 1, -3, -2});

		final List<int[]> subArrays = SubArrayWith0SumFinder.find(numbers);

		assertEquals(expectedSubArrays.size(), subArrays.size());
		for (int[] subArray : expectedSubArrays) {
			assertTrue(subArrays.stream().anyMatch(subArr -> Objects.deepEquals(subArr, subArray)));
			subArrays.remove(subArray);
		}
	}
}
